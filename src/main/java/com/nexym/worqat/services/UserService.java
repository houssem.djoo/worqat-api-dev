package com.nexym.worqat.services;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nexym.worqat.entities.Role;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.repositories.RoleRepository;
import com.nexym.worqat.repositories.UserRepository;

import net.bytebuddy.utility.RandomString;

@Service("userService")
@Transactional
public class UserService {
	private UserRepository userRepository;
	private RoleRepository roleRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public UserService(UserRepository userRepository, RoleRepository roleRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	public User findUserByEmail(String email) {
		return userRepository.findUserByEmail(email);
	}
	public User findUserByUsername(String username) {
		return userRepository.findUserByUsername(username);
	}

	public User findUserByConfirmationToken(String confirmationToken) {
		return userRepository.findUserByConfirmationToken(confirmationToken);
	}
	
	public void saveUser(User user) {
		String token = RandomString.make(30);
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setPasswordConfirmation(bCryptPasswordEncoder.encode(user.getPasswordConfirmation()));
		user.setActive(0);
		//DateAttibute date = new DateAttibute();
		Date Datedata = DateAttibute.getTime(new Date());
		/*Date ourdate = new Date(System.currentTimeMillis());
		String Datedata = ourdate.toString();*/
		user.setCreatedAt(Datedata.toString());
		user.setConfirmationToken(token);

		Role userRole = roleRepository.findByRole("USER");
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		userRepository.save(user);
	}

	public void updateResetPasswordToken(String token, String email) throws UserNotFoundException {
		User user = userRepository.findUserByEmail(email);
		if (user != null) {
			user.setResetPasswordToken(token);
			userRepository.save(user);
		} else {
			throw new UserNotFoundException("Could not find any user with the email " + email);
		}
	}
	

	public User getByResetPasswordToken(String token) {
		return userRepository.findByResetPasswordToken(token);
	}

	public void updatePassword(User user, String newPassword) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(newPassword);
		user.setPassword(encodedPassword);
		user.setResetPasswordToken(null);
		userRepository.save(user);
	}

	public String cryppass(String password) {
		return bCryptPasswordEncoder.encode(password);
	}
	
}