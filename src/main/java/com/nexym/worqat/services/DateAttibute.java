package com.nexym.worqat.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateAttibute {

	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
    public static Date getTime(Date currentDate) {
    	

        //Date currentDate = new Date();
        //System.out.println(dateFormat.format(currentDate));

        // convert date to calendar
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);

        // manipulate date
   /*     c.add(Calendar.YEAR, 1);
        c.add(Calendar.MONTH, 1);
        c.add(Calendar.DATE, 1); //same with c.add(Calendar.DAY_OF_MONTH, 1);
        c.add(Calendar.HOUR, 1);
        c.add(Calendar.MINUTE, 1);
        c.add(Calendar.SECOND, 1);
*/
        // convert calendar to date
        Date currentDatePlusOne = c.getTime();
       // System.out.println(dateFormat.format(currentDatePlusOne));
        return currentDatePlusOne;
    }
    
    public static Date getDate(String value) throws ParseException {
    	SimpleDateFormat formatter=new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy"); 
    	Date dateResult = formatter.parse(value);
    	return dateResult;
    }
    
    public static Date banThreeDays(Date date) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(date);
    	c.add(Calendar.DATE, 3);
    	Date bannedUntil = c.getTime();
    	return bannedUntil;
    }
    
    public static Date banSevenDays(Date date) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(date);
    	c.add(Calendar.DATE, 7);
    	Date bannedUntil = c.getTime();
    	return bannedUntil;
    }
    public static Date banFifteenDays(Date date) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(date);
    	c.add(Calendar.DATE, 15);
    	Date bannedUntil = c.getTime();
    	return bannedUntil;
    }
    public static Date banThirtyDays(Date date) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(date);
    	c.add(Calendar.DATE, 30);
    	Date bannedUntil = c.getTime();
    	return bannedUntil;
    }
    public static Date banPermantly(Date date) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(date);
    	c.add(Calendar.YEAR, 999);
    	Date bannedUntil = c.getTime();
    	return bannedUntil;
    }
    public static Date banseconds(Date date) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(date);
    	c.add(Calendar.SECOND, 5);
    	Date bannedUntil = c.getTime();
    	return bannedUntil;
    }
    

}
