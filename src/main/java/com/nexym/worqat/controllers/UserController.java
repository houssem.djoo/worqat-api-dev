package com.nexym.worqat.controllers;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.jaxb.SpringDataJaxb.SortDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.nexym.worqat.entities.Conversation;
import com.nexym.worqat.entities.Notification;
import com.nexym.worqat.entities.Role;
import com.nexym.worqat.entities.Skill;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.repositories.RoleRepository;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.services.DateAttibute;
import com.nexym.worqat.services.UserService;

@RestController
@RequestMapping({ "/users" })
@CrossOrigin(origins = "*")
public class UserController {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired 
	private UserService userService;
	
	@Autowired 
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private RoleRepository roleRepository;
	
	int i = 0;
	int reviewSize = 0;
	Double rating = 0.00;
	
	@GetMapping("/list")
	public List<User> getAllUsers() {
		List<User> users = userRepository.findAll();
		users.forEach((user)->{
			user.setConversations(null);
		});
		return users;
	}

	@PutMapping("/edit")
	public User updateUser(@Valid @RequestBody User userRequest, HttpServletRequest request, HttpServletResponse response ) throws Exception{
		
		String jwt = request.getHeader("Authorization").substring(7);
		DecodedJWT jwtd = JWT.decode(jwt);
		String usernameFromToken = jwtd.getSubject();
		Long userIdco = userService.findUserByUsername(usernameFromToken).getId();
			
		   return userRepository.findById(userIdco).map(user -> {
			   Set<Skill> userskills = userRequest.getSkills();
			  if( userRequest.getFirstName()!=null) user.setFirstName(userRequest.getFirstName());
			  if( userRequest.getLastName()!=null) user.setLastName(userRequest.getLastName());
			  if( userRequest.getPhoneNumber()!=null) user.setPhoneNumber(userRequest.getPhoneNumber());
			  if( userRequest.getNationality()!=null) user.setNationality(userRequest.getNationality());
			  if( userRequest.getEducation()!=null) user.setEducation(userRequest.getEducation());
			  if( userRequest.getGender()!=null) user.setGender(userRequest.getGender());
			  if( userRequest.getDescription()!=null) user.setDescription(userRequest.getDescription());
			  if( userRequest.getLanguages()!=null) user.setLanguages(userRequest.getLanguages());
			  if( userRequest.getSkills()!=null) user.setSkills(userskills);
			  if( userRequest.getImageUrl()!=null) user.setImageUrl(userRequest.getImageUrl());
			  if( userRequest.getPricePerHour()!=null) user.setPricePerHour(userRequest.getPricePerHour());
			  if (userRequest.getPassword()!=null && userRequest.getPasswordConfirmation()!=null && userRequest.getPassword().equals(userRequest.getPasswordConfirmation()) ) {
					user.setPassword(bCryptPasswordEncoder.encode(userRequest.getPassword()));
				}else if (userRequest.getPassword()==null && userRequest.getPasswordConfirmation()==null);
				else throw new RuntimeException("password doesn't match");
			  
			 /* if (user.getSkills()!=null)
				{
				Role userRole = roleRepository.findByRole("Freelancer");
				user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
			   }					*/
			
			return userRepository.save(user);
		
		}).orElseThrow(() -> new IllegalArgumentException("username " + usernameFromToken + " not found"));			
	}
	
	
	/*@DeleteMapping("/delete/{userId}")
	public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
		
		return userRepository.findById(userId).map(user -> {
			userRepository.delete(user);

			 
			return ResponseEntity.ok().body("User "+userId+" has been deleted");
		}).orElseThrow(() -> new IllegalArgumentException("userId " + userId + " not found"));
	}*/

	@GetMapping("/{userId}")
	public User getUserById(@PathVariable Long userId) {		
		User user = userRepository.findUsById(userId);
		if (user.getReviews().isEmpty()) {
			reviewSize = 1;
			rating=1.0;
		}else {
			user.getReviews().forEach((rev) -> {
				rating = rating + rev.getRate();
			});
			reviewSize = user.getReviews().size();
			rating=rating/reviewSize;
		}
		rating =Double.parseDouble(new DecimalFormat("#.#").format(rating));
		user.setRating(rating);
		userRepository.save(user);
		rating = 0.0;
		reviewSize=0;
		user.getReviews().forEach((rev)->{
			rev.setPhotoReviewer(userRepository.findUserByUsername(rev.getReviewer()).getImageUrl());
		});
		/*Notification[] tab = user.getNotifications().toArray(new Notification[user.getNotifications().size()]);
		for (int x = 0; x < tab.length - 1; x++)  
        {
             int index = x;  
             for (int j = x + 1; j < tab.length; j++)
             {
                  if (tab[j].getId() < tab[index].getId()){ 
                       index = j;
                  }
             }

             Notification min = tab[index];
             tab[index] = tab[x]; 
             tab[x] = min;
        }
		user.getNotifications().stream().sorted();
		*/
		//Collections.sort(user.getNotifications(), new SortNotificationByDate() );
		//Collections.reverse(user.getNotifications());
		return user;
	}
	
	@GetMapping("/user/{username}")
	public User getUserByUsername(@PathVariable String username) {
		return userRepository.findUserByUsername(username);
	}
	
	public Long userIdFromToken (String jwt) {
		
		DecodedJWT jwtd = JWT.decode(jwt);
		String usernameFromToken = jwtd.getSubject();
		Long userIdco = userService.findUserByUsername(usernameFromToken).getId();
		return userIdco;
	}
}

/*
class SortNotificationByDate implements Comparator<Notification>
{	
    // Used for sorting in Descending order of
    // roll number
    public int compare(Notification a,Notification b)
    {	try {
		Date d1 = DateAttibute.getDate(a.getDate());
		Date d2 = DateAttibute.getDate(b.getDate());
		if (d2.before(d1))
	        return -1;
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return 0;	
    }
}
class SortConversationByDate implements Comparator<Conversation>
{	
    // Used for sorting in Descending order of
    // roll number
    public int compare(Conversation a,Conversation b)
    {	Date d1 = a.getCreatedAt();
		Date d2 = a.getCreatedAt();
		if (d2.before(d1))
	    return -1;
	return 0;	
    }
}*/