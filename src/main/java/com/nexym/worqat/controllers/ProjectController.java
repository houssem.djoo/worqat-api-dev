package com.nexym.worqat.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import com.nexym.worqat.entities.Conversation;
import com.nexym.worqat.entities.Project;
import com.nexym.worqat.entities.SubmitsRequest;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.payload.response.MessageResponse;
import com.nexym.worqat.repositories.ConversationRepository;
import com.nexym.worqat.repositories.ProjectRepository;
import com.nexym.worqat.repositories.SubmitsRepository;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.services.DateAttibute;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping({ "/projects" })
public class ProjectController {
	private final ProjectRepository projectRepository;
	private final UserRepository userRepository;
	
	@Autowired
	private UserController userController;
	
	@Autowired
	private SubmitsRepository submitsRepository;
	
	@Autowired
	private ConversationRepository conversationRepository;
	
	@Autowired
	public ProjectController(ProjectRepository projectRepository, UserRepository userRepository) {
		this.projectRepository = projectRepository;
		this.userRepository = userRepository;
	}
	 
	@GetMapping("/list")
	public List<Project> getAllProjects() {
		List<Project> projectList = projectRepository.findAll();
		projectList.forEach((pr)->{
			pr.setFreelancerAffected(null);
			pr.setFreelancersSubmits(null);
			pr.setUser(null);
		});
		return projectList;
	}
	
	@GetMapping("/project/{projectId}")
	public Project getProject(@PathVariable(value = "projectId") long projectId) {
		Project project = projectRepository.findProjectById(projectId);
		project.setFreelancersSubmits(null);
		project.getUser().setConversations(null);
		return project;
	}
	
	@GetMapping("/list/submitslist/{projectId}")
	public MessageResponse getSubmittedProject(@PathVariable (value = "projectId") long projectId ,HttpServletRequest request){
		Project project = projectRepository.findProjectById(projectId);
		/*String jwt = request.getHeader("Authorization").substring(7);
		Long uid = userController.userIdFromToken(jwt);
		if (project.getUser().getId()!=uid) throw new RuntimeException("SECURITY : This token not of this project owner");*/
		Set<SubmitsRequest> list = project.getFreelancersSubmits();
		list.forEach((pr)->{
			pr.getUser().setConversations(null);
			pr.getUser().setReviews(null);
			pr.getUser().setSkills(null);
			pr.getUser().setPortfolios(null);
			pr.getUser().setProjects(null);
			pr.getUser().setNotifications(null);
		});
		MessageResponse result = new MessageResponse(project.getFreelancerAffected(),project.getFreelancersSubmits());
		return  result;
	}
	
	@GetMapping("/list/{userId}/{status}")
	public List<Project> getMyProjectsAsHolder(@PathVariable (value = "userId") long userId,@PathVariable (value = "status") String status,HttpServletRequest request ){
		/*String jwt = request.getHeader("Authorization").substring(7);
		Long uid = userController.userIdFromToken(jwt);
		if (userId!=uid) throw new RuntimeException("SECURITY : This token not of those projects owner");*/
		User user = userRepository.findUsById(userId);
		List<Project> projectList = new ArrayList<>();
		user.getProjects().forEach((pr)->{
			
			pr.setFreelancerAffected(null);
			pr.setFreelancersSubmits(null);
			pr.getUser().setConversations(null);
			projectList.add(pr);
		});
		if(status.contains("all")) return projectList;
		List<Project> projectReturn = new ArrayList<>();
			projectList.forEach((project)->{
				if (project.getStatus().contains(status))
					projectReturn.add(project);
			});
		return projectReturn;
	}
	
	@GetMapping("/myfreelancersproject/{userId}/{status}")
	public List<Project> getMyProjectsAsFreelancer(@PathVariable (value = "userId") long userId,@PathVariable (value = "status") String status,HttpServletRequest request ) throws Exception{
		/*String jwt = request.getHeader("Authorization").substring(7);
		Long uid = userController.userIdFromToken(jwt);
		if (userId!=uid) throw new RuntimeException("SECURITY : Bad token for this request");*/
		User user = userRepository.findUsById(userId);
		List<Project> myProjectsAsf = new ArrayList<>();
		List<Project> projectsReturn = new ArrayList<>();
		List<Project> projects = projectRepository.findAll();
		projects.forEach((info)-> {
			if (info.getFreelancerAffected() == user.getUsername()) myProjectsAsf.add(info); 
			else info.getFreelancersSubmits().forEach((search)-> {
				if(search.getUser()==user) {
					info.setFreelancersSubmits(null);
					info.getUser().setConversations(null);
					myProjectsAsf.add(info);
				}
				});
		});
			if(status.contains("all")) return myProjectsAsf;
		myProjectsAsf.forEach((project)->{
			if (project.getStatus().contains(status))
				projectsReturn.add(project);
		});
		
		return projectsReturn;
	}
	
	@PostMapping("/add")
	Project createProject(@Valid @RequestBody Project project,HttpServletRequest request) throws Exception {
		String jwt = request.getHeader("Authorization").substring(7);
		Long uid = userController.userIdFromToken(jwt);
		
		return userRepository.findById(uid).map(user -> {
			project.setUser(user);
			project.setStatus("pending");
			Date createdAt = DateAttibute.getTime(new Date());
			project.setCreatedAt(createdAt.toString());
			Calendar cc = Calendar.getInstance();
			cc.setTime(createdAt);
			cc.add(Calendar.YEAR,9999);
			project.setBeginningWork(cc.getTime());
			return projectRepository.save(project);
		}).orElseThrow(() -> new IllegalArgumentException("SECURITY : Bad token ! you cannot add project !"));
	}
	
	@PutMapping("/update/{projectId}")
	Project updateProject(@PathVariable(value = "projectId") long projectId,@Valid @RequestBody Project project,HttpServletRequest request) throws Exception {
		//String jwt = request.getHeader("Authorization").substring(7);
		//Long uid = userController.userIdFromToken(jwt);
		Project proj = projectRepository.findProjectById(projectId);
		
		//if (proj.getUser().getId()!=uid) throw new RuntimeException("SECURITY : This token is not of project owner");
		
		if (proj.getFreelancerAffected()==null) {
			proj.setTitle(project.getTitle());
			proj.setDescription(project.getDescription());
			proj.setCategory(project.getCategory());
			proj.setDuration(project.getDuration());
			proj.setPrice(project.getPrice());
			proj.setSkillsReq(project.getSkillsReq());
			proj.setAttachmentUrls(project.getAttachmentUrls());
		}else throw new RuntimeException("This project is affected to a freelancer you can't edit it");

		return projectRepository.save(proj);
	}
	
	@DeleteMapping("delete/{id}")
	public String deleteProject(@PathVariable("id") long id,HttpServletRequest request) {
		Project project = projectRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid project Id:" + id));
		//String jwt = request.getHeader("Authorization").substring(7);
		//Long uid = userController.userIdFromToken(jwt);			
		//if (project.getUser().getId()!=uid) throw new RuntimeException("SECURITY : This token is not of project owner");
		
		
		if (project.getFreelancerAffected()!=null) throw new RuntimeException("This project is affected to a freelancer you can't delete it");
		Set<SubmitsRequest> subList = project.getFreelancersSubmits();	
					project.removeAllSubmits();
					subList.forEach((sb)->{		
						submitsRepository.delete(sb);
					});
					project.removeAllSkills();
					projectRepository.save(project);
					System.out.println("REMOVED ALL SUBMITS");
					System.out.println("REMOVED ALL SUBMITS");
					System.out.println("REMOVED ALL SUBMITS");
					System.out.println("REMOVED ALL SUBMITS");
					System.out.println("REMOVED ALL SUBMITS");
					System.out.println("REMOVED ALL SUBMITS");
					
		projectRepository.delete(project);
		
		System.out.println("GOING TO DELETE SUBMITS SUBMITS");
		System.out.println("GOING TO DELETE SUBMITS SUBMITS");
		System.out.println("GOING TO DELETE SUBMITS SUBMITS");
		System.out.println("GOING TO DELETE SUBMITS SUBMITS");
		System.out.println("GOING TO DELETE SUBMITS SUBMITS");
		System.out.println("GOING TO DELETE SUBMITS SUBMITS");
		System.out.println("GOING TO DELETE SUBMITS SUBMITS");
		
	 return "Project "+id+" has been deleted";	
	}
	
				/************************                      ******************************
				 * ############################ SUBMIT API'S ################################ *
				 * **********************                      **************************** *
				#########################################################################**/
	
	
	@PostMapping("/submit/{userId}/{projectId}")
	Project submitRequestFreelancer(@PathVariable(value = "userId") Long userId,@PathVariable(value = "projectId") Long projectId, @Valid @RequestBody SubmitsRequest
			submitRequest,HttpServletRequest request) throws Exception {
		//String jwt = request.getHeader("Authorization").substring(7);
		//Long uid = userController.userIdFromToken(jwt);			
		//if (userId!=uid) throw new RuntimeException("SECURITY : This token is not of user request");
		User userSubmit = userRepository.findUsById(userId);
		if (userSubmit==null) throw new RuntimeException("Invalid userId");
		Project projectUpdate = projectRepository.findProjectById(projectId);
		if (projectUpdate == null) throw new RuntimeException("Project "+projectId+" doesn't exist");
		if (userSubmit == projectUpdate.getUser()) throw new RuntimeException("This user is the owner of project "+projectId);
		if (projectUpdate.getFreelancerAffected()!=null) throw new RuntimeException("Sorry ! This project is already affected to a freelancer !");
		Set<SubmitsRequest> submitsList = projectUpdate.getFreelancersSubmits();
		
		submitsList.forEach((info) -> {
			if (info.getUser()==userSubmit) throw new RuntimeException("User already submitted");
		});
		
		String required = projectUpdate.getSkillsReq().toString();
		userSubmit.getSkills().forEach((sk)->{
    		String skillUser = ".*"+sk.getName()+".*";
    		if(required.matches(skillUser)) {
    			SubmitsRequest newSubmit = submitRequest;
    			newSubmit.setUser(userSubmit);
    			submitsRepository.save(newSubmit);
    			submitsList.add(newSubmit);
    			projectUpdate.setFreelancersSubmits(submitsList);
    			projectRepository.save(projectUpdate);
    		}    				
    	});
		if (!projectUpdate.getFreelancersSubmits().stream().anyMatch(sb -> sb.getUser().equals(userSubmit))) throw new RuntimeException("You don't have any skill required on this project !");
			
		
		
		return projectUpdate;
		
	}
	
	@PostMapping("/affect/{userId}/{projectId}")
	Project affectFreelancerToProject(@PathVariable(value = "userId") Long userId, @PathVariable(value = "projectId") Long projectId,HttpServletRequest request) throws Exception{
		Project project = projectRepository.findProjectById(projectId);
		//String jwt = request.getHeader("Authorization").substring(7);
		//Long uid = userController.userIdFromToken(jwt);			
		//if (project.getUser().getId()!=uid) throw new RuntimeException("SECURITY : This token is not of project owner");
		Set<SubmitsRequest> submitsList = project.getFreelancersSubmits();
		User freelancer = userRepository.findUsById(userId);
		
		if (submitsList.stream().anyMatch(sl -> sl.getUser().equals(freelancer))) {
		project.setFreelancerAffected(freelancer.getUsername());
		project.setStatus("working on");
		freelancer.setNbProjectsWorked(freelancer.getNbProjectsWorked()+1);
		Conversation newConv = new Conversation();
		newConv.getUsers().add(freelancer);
		newConv.getUsers().add(project.getUser());
		String nameConv = freelancer.getUsername().concat(projectId.toString());
		newConv.setConversationName(nameConv);
		newConv.setCreatedAt(DateAttibute.getTime(new Date()));
		newConv.setFreelancerName(freelancer.getUsername());
		newConv.setOwnerName(project.getUser().getUsername());
		newConv.setProjectName(project.getId()+project.getTitle());
		project.setBeginningWork(new Date());
		conversationRepository.save(newConv);
		return projectRepository.save(project);
		
		} else if (freelancer==null || project==null) {throw new RuntimeException("userId " + userId + " not found or bad projectId");
		
	} else throw new RuntimeException("user doesn't exist on submits list");
	}
	
	@PutMapping("/update/submit/{userId}/{projectId}")
	String updateSubmit(@PathVariable(value = "userId") long userId,@PathVariable(value = "projectId") long projectId,@Valid @RequestBody SubmitsRequest submitRequest,HttpServletRequest request) throws Exception {
		Project project = projectRepository.findProjectById(projectId);
		//String jwt = request.getHeader("Authorization").substring(7);
		//Long uid = userController.userIdFromToken(jwt);			
		//if (userId!=uid) throw new RuntimeException("SECURITY : This token is not the owner of this submit");
		User user = userRepository.findUsById(userId);
		if (project.getFreelancerAffected()!=null) throw new RuntimeException("You cannot update this submit cause project status");
	
		project.getFreelancersSubmits().forEach((sb)->{
			
			if (sb.getUser().getId()==user.getId())
			{
				sb.setPricePerHour(submitRequest.getPricePerHour());
				sb.setWeeklyHours(submitRequest.getWeeklyHours());
				sb.setMessageDescription(submitRequest.getMessageDescription());
				submitsRepository.save(sb);
			}else throw new RuntimeException("This user isn't submitted on this project");
			
		});
		return "Submit updated" ;	 
	}
	
	@DeleteMapping("delete/submit/{userId}/{projectId}")
	public String deleteSubmitRequest(@PathVariable("userId") long userId,@PathVariable("projectId") long projectId,HttpServletRequest request) {
		//String jwt = request.getHeader("Authorization").substring(7);
		//Long uid = userController.userIdFromToken(jwt);			
		//if (userId!=uid) throw new RuntimeException("SECURITY : This token is not the owner of this submit");
		List<SubmitsRequest> subRequest = submitsRepository.findByUser(userRepository.findUsById(userId));
				if (subRequest==null) throw new RuntimeException("Invalid user Id:" + userId);
		Project projRequest = projectRepository.findById(projectId)
				.orElseThrow(() -> new IllegalArgumentException("Invalid project Id:" + projectId));
		if (projRequest.getFreelancerAffected()==null) throw new RuntimeException("You cannot delete this submit cause we have affected project to a freelancer");
		projRequest.getFreelancersSubmits().forEach((sb)->{
			subRequest.forEach((sbpr)->{
				if (sb.getId()== sbpr.getId());
				});			
				projRequest.removeSubmit(sb);
				projectRepository.save(projRequest);
				submitsRepository.delete(sb);						
		});
		return "Submit has been deleted";	
	 
	}

}