package com.nexym.worqat.controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nexym.worqat.entities.Role;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.repositories.RoleRepository;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.services.UserService;
@Controller
@RequestMapping("/accounts/")
public class AccountController {
	private final UserRepository userRepository;
	private final RoleRepository roleRepository;
	private final UserService userService;
	
	@Autowired
	public AccountController(UserRepository userRepository, RoleRepository roleRepository, UserService userService) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.userService = userService;
	}
	
	@GetMapping("list")
	public String listUsers(Model model) {

		List<User> users = (List<User>) userRepository.findAll();
		long nbr = userRepository.count();
		if (users.size() == 0)
			users = null;
		model.addAttribute("users", users);
		model.addAttribute("nbr", nbr);
		return "user/listUsers";
	}

	@RequestMapping("/enable")
	public void enableUserAcount(@Param(value="confirmation") HttpServletRequest request,HttpServletResponse response) throws MessagingException, IOException {
		
		User user = userService.findUserByConfirmationToken(request.getParameter("confirmation"));
		if (user!=null)
		{user.setActive(1);
		user.setConfirmationToken(null);
		userRepository.save(user);
		String worqat = "http://www.worqat.com";
		response.sendRedirect(worqat);
	}else throw new RuntimeException("No user token sent to activate");
	}


	@PostMapping("updateRole")
	public String UpdateUserRole(@RequestParam("id") long id, @RequestParam("newrole") String newRole) {

		User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));
		Role userRole = roleRepository.findByRole(newRole);
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		userRepository.save(user);
		return "redirect:list";
	}
}
