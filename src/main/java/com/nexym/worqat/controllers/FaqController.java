package com.nexym.worqat.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.Banner;
import com.nexym.worqat.entities.Faq;
import com.nexym.worqat.entities.WhyWorqat;
import com.nexym.worqat.payload.request.BannerRequest;
import com.nexym.worqat.payload.request.FaqRequest;
import com.nexym.worqat.repositories.BannerRepository;
import com.nexym.worqat.repositories.FaqRepository;
import com.nexym.worqat.repositories.WhyWorqatRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("global")
public class FaqController {
	@Autowired
	private FaqRepository faqRepository;
	
	@Autowired
	private BannerRepository bannerRepository;
	
	@Autowired
	private WhyWorqatRepository whyWorqatRepository;
	
	@GetMapping("/faq/show")
	public Faq getFaq() {
		return faqRepository.findAll().get(0);
	}
	
	@DeleteMapping("/faq/reset")
	public String deleteFaq() {
		faqRepository.delete(faqRepository.findAll().get(0));
		return "Faq has been deleted , will be reinitialized when refresh the home page !";
	}
	
	@PutMapping("/faq/block1")
	public String editBlock1(@RequestBody FaqRequest faqReq) {
		Faq faq = faqRepository.findAll().get(0);
		faq.setTitle1(faqReq.getTitle1());
		faq.setTitle2(faqReq.getTitle2());
		faq.setTitle3(faqReq.getTitle3());
		faq.setDescription1(faqReq.getDescription1());
		faq.setDescription2(faqReq.getDescription2());
		faq.setDescription3(faqReq.getDescription3());
		if (faqReq.getTitle1() != null && faqReq.getTitle2() != null && faqReq.getTitle3() != null 
			&&	faqReq.getDescription1() != null && faqReq.getDescription2() != null && faqReq.getDescription3() != null	)
		{
			faqRepository.save(faq);
		}else throw new RuntimeException("You must complete the required field");
		return "update successfully";
	}
	
	@PutMapping("/faq/block2")
	public String editBlock2(@RequestBody FaqRequest faqReq  ) {
		Faq faq = faqRepository.findAll().get(0);
		faq.setTitle4(faqReq.getTitle4());
		faq.setTitle5(faqReq.getTitle5());
		faq.setTitle6(faqReq.getTitle6());
		faq.setDescription4(faqReq.getDescription4());
		faq.setDescription5(faqReq.getDescription5());
		faq.setDescription6(faqReq.getDescription6());
		if (faqReq.getTitle4() != null && faqReq.getTitle5() != null && faqReq.getTitle6() != null 
				&&	faqReq.getDescription4() != null && faqReq.getDescription5() != null && faqReq.getDescription6() != null	)
			{
				faqRepository.save(faq);
			}else throw new RuntimeException("You must complete the required field");
		return "update successfully";
	}
	
	@PutMapping("/faq/block3")
	public String editBlock3(@RequestBody FaqRequest faqReq  ) {
		Faq faq = faqRepository.findAll().get(0);
		faq.setTitle7(faqReq.getTitle7());
		faq.setTitle8(faqReq.getTitle8());
		faq.setDescription7(faqReq.getDescription7());
		faq.setDescription8(faqReq.getDescription8());
		if (faqReq.getTitle7() != null && faqReq.getTitle8() != null 
				&&	faqReq.getDescription7() != null && faqReq.getDescription8() != null )
			{
				faqRepository.save(faq);
			}else throw new RuntimeException("You must complete the required field");
		return "update successfully";
	}
	
	@PutMapping("/faq/block4")
	public String editBlock4(@RequestBody FaqRequest faqReq  ) {
		Faq faq = faqRepository.findAll().get(0);
		faq.setTitle9(faqReq.getTitle9());
		faq.setTitle10(faqReq.getTitle10());
		faq.setDescription9(faqReq.getDescription9());
		faq.setDescription10(faqReq.getDescription10());
		if (faqReq.getTitle9() != null && faqReq.getTitle10() != null
				&&	faqReq.getDescription9() != null && faqReq.getDescription10() != null)
			{
				faqRepository.save(faq);
			}else throw new RuntimeException("You must complete the required field");
		return "update successfully";
	}
	
	@PutMapping("/whyworqat/edit")
	public String editWhyWorqat(@RequestBody WhyWorqat whyworqat  ) {
		WhyWorqat whyworqat1 = whyWorqatRepository.findAll().get(0);
		whyworqat1.setDescription1(whyworqat.getDescription1());
		whyworqat1.setDescription2(whyworqat.getDescription2());
		whyworqat1.setDescription3(whyworqat.getDescription3());
		whyworqat1.setDescription4(whyworqat.getDescription4());
		if (whyworqat1.getDescription1() != null && whyworqat1.getDescription2() != null
				&& whyworqat1.getDescription3() != null 
				&&	whyworqat1.getDescription4() != null )
			{
			whyWorqatRepository.save(whyworqat1);
			}else throw new RuntimeException("You must complete the required field");
		return "update successfully";
	}
	
	@GetMapping("/whyworqat/show")
	public WhyWorqat getWhyWorqat() {
		return whyWorqatRepository.findAll().get(0);
	}
	
	@PutMapping("/banner/edit")
	public String editBanner(@RequestBody BannerRequest bannerReq) {
		Banner banner = bannerRepository.findAll().get(0);
		if (!bannerReq.getBannerUrls().isEmpty()) {
		banner.setBannerUrls(bannerReq.getBannerUrls());
		bannerRepository.save(banner);
		return "Banner urls updated";
	}else return "You must remplish the banner !";
		
	}
	@GetMapping("/banner/show")
	public Banner getBanner() {
		return bannerRepository.findAll().get(0);
	}

}
