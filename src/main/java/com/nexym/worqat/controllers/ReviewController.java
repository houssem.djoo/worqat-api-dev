package com.nexym.worqat.controllers;

import java.text.DecimalFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.Comment;
import com.nexym.worqat.entities.Review;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.repositories.CommentRepository;
import com.nexym.worqat.repositories.ReviewRepository;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.services.DateAttibute;

@Transactional
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/review")
public class ReviewController {
	
	@Autowired
	private ReviewRepository reviewRepository;
	
	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired 
	private UserRepository userRepository;

	int i = 0;
	int reviewSize = 0;
	Double newRating = 0.00;
	Double test = 0.00;
	
	@PostMapping("/add/{userId}")
	public String addReview(@PathVariable(value = "userId") Long userId , @RequestBody Review review,HttpServletRequest request ) {
				
		User user = userRepository.findUsById(userId);
				user.getProjects().forEach((project)->{ 
					if(project.getStatus().contains("closed"))
					if(project.getFreelancerAffected().contains(review.getReviewer()))
						i++;
				});
				
			if (i<1) throw new RuntimeException("SECURITY : This reviewer has never worked for "+user.getUsername());
				review.setUser(user);
				review.setCreatedAt(DateAttibute.getTime(new Date()).toString());
				review.setPhotoReviewer(userRepository.findUserByUsername(review.getReviewer()).getImageUrl());
				reviewRepository.save(review);
				
				if (user.getReviews().isEmpty()) {
					reviewSize = 1;
					newRating=(double)review.getRate();
				}else {
					
					reviewSize = user.getReviews().size()+1;
					newRating=(review.getRate()+(user.getRating()*user.getReviews().size()))/reviewSize;
				}
				newRating =Double.parseDouble(new DecimalFormat("#.#").format(newRating));
				user.setRating(newRating);
				userRepository.save(user);
				
				//newRating=0.0;
				i=0;
				return "rating = "+newRating;
	}
	
	@PostMapping("/comment/{reviewId}")
	public Comment addComment(@PathVariable(value = "reviewId") Long reviewId, @RequestBody Comment comment) {
				Review review = reviewRepository.getById(reviewId);
				if (comment.getCommentOwner().contains(review.getReviewer()) || comment.getCommentOwner().contains(review.getUser().getUsername()) ) {
					comment.setReview(review);
					comment.setCreatedAt(DateAttibute.getTime(new Date()).toString());
				} else throw new RuntimeException("SECURITY : This comment owner cannot comment this review");
				
		return commentRepository.save(comment);
	}
	
	@DeleteMapping("/delete/{reviewId}")
	public String deleteReview(@PathVariable(value = "reviewId") Long reviewId) {
		Review review = reviewRepository.getById(reviewId);
		reviewRepository.delete(review);
		return "Review has been deleted";
	}
	
	
}
