package com.nexym.worqat.controllers;

import java.text.ParseException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.Historic;
import com.nexym.worqat.entities.Project;
import com.nexym.worqat.entities.Ticket;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.entities.VisitorsNumber;
import com.nexym.worqat.repositories.HistoricRepository;
import com.nexym.worqat.repositories.ProjectRepository;
import com.nexym.worqat.repositories.RoleRepository;
import com.nexym.worqat.repositories.TicketRepository;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.repositories.VisitorsRepository;
import com.nexym.worqat.services.DateAttibute;

@RestController
@RequestMapping("stats")
public class StatsController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired 
	private TicketRepository ticketRepository;
	
	@Autowired 
	private VisitorsRepository visitRepo;
	
	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private HistoricRepository historicRepository;
	
	@GetMapping("/info")
	public Stats getStats() throws ParseException {
		Stats stats = new Stats();
		
		List<User> usersList = userRepository.findAll()	;
		List<Project> projectsList = projectRepository.findAll();
		List<Ticket> ticketsList = ticketRepository.findAll();
		
		stats.nbTotalUsers=usersList.size();
		stats.nbUsers=userRepository.findUserByRoles(roleRepo.findByRole("USER")).size();
		stats.nbFreelancers=userRepository.findUserByRoles(roleRepo.findByRole("FREELANCER")).size();
		stats.nbAdmins=userRepository.findUserByRoles(roleRepo.findByRole("ADMIN")).size();
		
		stats.nbTotalProjects=projectsList.size();
		List<User> st = userRepository.findTopUsers();

		stats.rate1= st.get(0).getRating(); 
		stats.rate2= st.get(1).getRating(); 
		stats.rate3= st.get(2).getRating(); 
		stats.rate4= st.get(3).getRating(); 
		stats.rate5= st.get(4).getRating(); 
		stats.imageUrl1=st.get(0).getImageUrl();
		stats.imageUrl2=st.get(1).getImageUrl();
		stats.imageUrl3=st.get(2).getImageUrl();
		stats.imageUrl4=st.get(3).getImageUrl();
		stats.imageUrl5=st.get(4).getImageUrl();
		stats.pricePerHour1=st.get(0).getPricePerHour();
		stats.pricePerHour2=st.get(1).getPricePerHour();
		stats.pricePerHour3=st.get(2).getPricePerHour();
		stats.pricePerHour4=st.get(3).getPricePerHour();
		stats.pricePerHour5=st.get(4).getPricePerHour();
		stats.nbprojects1=st.get(0).getNbProjectsWorked();
		stats.nbprojects2=st.get(1).getNbProjectsWorked();
		stats.nbprojects3=st.get(2).getNbProjectsWorked();
		stats.nbprojects4=st.get(3).getNbProjectsWorked();
		stats.nbprojects5=st.get(4).getNbProjectsWorked();
		stats.topFreelancer1 = st.get(0).getUsername();
		stats.topFreelancer2 = st.get(1).getUsername();
		stats.topFreelancer3 = st.get(2).getUsername();
		stats.topFreelancer4 = st.get(3).getUsername();
		stats.topFreelancer5 = st.get(4).getUsername();
	
			Date currentDate = new Date();
			Calendar current = Calendar.getInstance();
			current.setTime(currentDate);
			if (historicRepository.findHistByYearAndProjects(current.get(Calendar.YEAR),"all")==null ) {
				Historic allhist = new Historic(current.get(Calendar.YEAR), "all", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0); 
				historicRepository.save(allhist);
				Historic workhist = new Historic(current.get(Calendar.YEAR), "working", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0); 
				historicRepository.save(workhist);
			}
			
		List<Historic> history = historicRepository.findAll();
		
		for ( int j = 0 ; j < history.size() ; j++ ) {
			Historic histoire = history.get(j) ; 		
			histoire.setJanuary(0);
			histoire.setFebruary(0);
			histoire.setMarch(0);
			histoire.setApril(0);
			histoire.setMay(0);
			histoire.setJune(0);
			histoire.setJuly(0);
			histoire.setAugust(0);
			histoire.setSeptember(0);
			histoire.setOctober(0);
			histoire.setNovember(0);
			histoire.setDecember(0);
			if (histoire.getProjects().contains("all")) {
				projectsList.forEach((project)->{
					try {
						Date d = DateAttibute.getDate(project.getCreatedAt());
						Calendar cp = Calendar.getInstance();
						cp.setTime(d);
						
						if (cp.get(Calendar.YEAR) == histoire.getYear()) {
							if (cp.get(Calendar.MONTH) == 0) histoire.setJanuary(histoire.getJanuary()+1);
							if (cp.get(Calendar.MONTH) == 1) histoire.setFebruary(histoire.getFebruary()+1);
							if (cp.get(Calendar.MONTH) == 2) histoire.setMarch(histoire.getMarch()+1);
							if (cp.get(Calendar.MONTH) == 3) histoire.setApril(histoire.getApril()+1);
							if (cp.get(Calendar.MONTH) == 4) histoire.setMay(histoire.getMay()+1);
							if (cp.get(Calendar.MONTH) == 5) histoire.setJune(histoire.getJune()+1);
							if (cp.get(Calendar.MONTH) == 6) histoire.setJuly(histoire.getJuly()+1);
							if (cp.get(Calendar.MONTH) == 7) histoire.setAugust(histoire.getAugust()+1);
							if (cp.get(Calendar.MONTH) == 8) histoire.setSeptember(histoire.getSeptember()+1);
							if (cp.get(Calendar.MONTH) == 9) histoire.setOctober(histoire.getOctober()+1);
							if (cp.get(Calendar.MONTH) == 10) histoire.setNovember(histoire.getNovember()+1);
							if (cp.get(Calendar.MONTH) == 11) histoire.setDecember(histoire.getDecember()+1);					
						}
						historicRepository.save(histoire);
						
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				});	
			}else if (histoire.getProjects().contains("working")) {
				projectsList.forEach((project)->{
					Date d2 = project.getBeginningWork();
					Calendar cp = Calendar.getInstance();
					cp.setTime(d2);
					
					if (cp.get(Calendar.YEAR) == histoire.getYear()) {
						if (cp.get(Calendar.MONTH) == 0) histoire.setJanuary(histoire.getJanuary()+1);
						if (cp.get(Calendar.MONTH) == 1) histoire.setFebruary(histoire.getFebruary()+1);
						if (cp.get(Calendar.MONTH) == 2) histoire.setMarch(histoire.getMarch()+1);
						if (cp.get(Calendar.MONTH) == 3) histoire.setApril(histoire.getApril()+1);
						if (cp.get(Calendar.MONTH) == 4) histoire.setMay(histoire.getMay()+1);
						if (cp.get(Calendar.MONTH) == 5) histoire.setJune(histoire.getJune()+1);
						if (cp.get(Calendar.MONTH) == 6) histoire.setJuly(histoire.getJuly()+1);
						if (cp.get(Calendar.MONTH) == 7) histoire.setAugust(histoire.getAugust()+1);
						if (cp.get(Calendar.MONTH) == 8) histoire.setSeptember(histoire.getSeptember()+1);
						if (cp.get(Calendar.MONTH) == 9) histoire.setOctober(histoire.getOctober()+1);
						if (cp.get(Calendar.MONTH) == 10) histoire.setNovember(histoire.getNovember()+1);
						if (cp.get(Calendar.MONTH) == 11) histoire.setDecember(histoire.getDecember()+1);					
					}
					historicRepository.save(histoire);
					
				});	
			}
			
			
		}
		
		stats.historique = historicRepository.findAll();
			
		projectsList.forEach((pr)-> {
			if (pr.getStatus().contains("pending")) {
			stats.nbPendingProjects++;
			}else if (pr.getStatus().contains("working on")) {
				stats.nbWorkingOnProjects++;
			}
			else if (pr.getStatus().contains("in despute")) {
				stats.nbInDesputeProjects++;
			}else if (pr.getStatus().contains("closed")) {
				stats.nbClosedProjects++;
			}
		});
		
		stats.nbTotalTickets=ticketsList.size();
		ticketsList.forEach((ticket)-> {
			if (ticket.getStatus().contains("open")) {
				stats.nbOpenTickets++;
				}else if (ticket.getStatus().contains("pending")) {
					stats.nbPendingTickets++;
				}
				else if (ticket.getStatus().contains("resolved")) {
					stats.nbResolvedTickets++;
				}else if (ticket.getStatus().contains("closed")) {
					stats.nbClosedTickets++;
				}
		});
		long id = 99999;
		VisitorsNumber visits = visitRepo.getById(id) ;
		stats.nbVisitors=visits.getVistorsNumber();
		
		
		
		return stats;
	}
	
	public class Stats {
		public long nbVisitors;
		public int nbTotalUsers;
		public int nbUsers;
		public int nbFreelancers;
		public int nbAdmins;
		
		public int nbTotalProjects;
		public int nbPendingProjects;
		public int nbWorkingOnProjects;
		public int nbInDesputeProjects;
		public int nbClosedProjects;
		
		public int nbTotalTickets;
		public int nbOpenTickets;
		public int nbPendingTickets;
		public int nbResolvedTickets;
		public int nbClosedTickets;
		public String topFreelancer1;
		public double rate1;
		public int nbprojects1;
		public double pricePerHour1;
		public String imageUrl1;
		public String topFreelancer2;
		public double rate2;
		public int nbprojects2;
		public double pricePerHour2;
		public String imageUrl2;
		public String topFreelancer3;
		public double rate3;
		public int nbprojects3;
		public double pricePerHour3;
		public String imageUrl3;
		public String topFreelancer4;
		public String imageUrl4;
		public double rate4;
		public double pricePerHour4;
		public int nbprojects4;
		public String topFreelancer5;
		public double rate5;
		public int nbprojects5;
		public double pricePerHour5;
		public String imageUrl5;

		public List<Historic> historique;

		public Stats(long nbVisitors, int nbTotalUsers, int nbUsers, int nbFreelancers, int nbAdmins,
				int nbTotalProjects, int nbPendingProjects, int nbWorkingOnProjects, int nbInDesputeProjects,
				int nbClosedProjects, int nbTotalTickets, int nbOpenTickets, int nbPendingTickets,
				int nbResolvedTickets, int nbClosedTickets, String topFreelancer1, double rate1, int nbprojects1,
				double pricePerHour1, String imageUrl1, String topFreelancer2, double rate2, int nbprojects2,
				double pricePerHour2, String imageUrl2, String topFreelancer3, double rate3, int nbprojects3,
				double pricePerHour3, String imageUrl3, String topFreelancer4, String imageUrl4, double rate4,
				double pricePerHour4, int nbprojects4, String topFreelancer5, double rate5, int nbprojects5,
				double pricePerHour5, String imageUrl5, List<Historic> historique) {
			super();
			this.nbVisitors = nbVisitors;
			this.nbTotalUsers = nbTotalUsers;
			this.nbUsers = nbUsers;
			this.nbFreelancers = nbFreelancers;
			this.nbAdmins = nbAdmins;
			this.nbTotalProjects = nbTotalProjects;
			this.nbPendingProjects = nbPendingProjects;
			this.nbWorkingOnProjects = nbWorkingOnProjects;
			this.nbInDesputeProjects = nbInDesputeProjects;
			this.nbClosedProjects = nbClosedProjects;
			this.nbTotalTickets = nbTotalTickets;
			this.nbOpenTickets = nbOpenTickets;
			this.nbPendingTickets = nbPendingTickets;
			this.nbResolvedTickets = nbResolvedTickets;
			this.nbClosedTickets = nbClosedTickets;
			this.topFreelancer1 = topFreelancer1;
			this.rate1 = rate1;
			this.nbprojects1 = nbprojects1;
			this.pricePerHour1 = pricePerHour1;
			this.imageUrl1 = imageUrl1;
			this.topFreelancer2 = topFreelancer2;
			this.rate2 = rate2;
			this.nbprojects2 = nbprojects2;
			this.pricePerHour2 = pricePerHour2;
			this.imageUrl2 = imageUrl2;
			this.topFreelancer3 = topFreelancer3;
			this.rate3 = rate3;
			this.nbprojects3 = nbprojects3;
			this.pricePerHour3 = pricePerHour3;
			this.imageUrl3 = imageUrl3;
			this.topFreelancer4 = topFreelancer4;
			this.imageUrl4 = imageUrl4;
			this.rate4 = rate4;
			this.pricePerHour4 = pricePerHour4;
			this.nbprojects4 = nbprojects4;
			this.topFreelancer5 = topFreelancer5;
			this.rate5 = rate5;
			this.nbprojects5 = nbprojects5;
			this.pricePerHour5 = pricePerHour5;
			this.imageUrl5 = imageUrl5;
			this.historique = historique;
		}




		public Stats() {
			super();
			// TODO Auto-generated constructor stub
		}

	}	
	
}
