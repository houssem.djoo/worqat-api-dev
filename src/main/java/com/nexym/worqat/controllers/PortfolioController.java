package com.nexym.worqat.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.Portfolio;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.repositories.PortfolioRepository;
import com.nexym.worqat.repositories.UserRepository;

@RestController
@RequestMapping("/portfolio")
@Transactional
public class PortfolioController {
	
	private PortfolioRepository portfolioRepository;
	private UserRepository userRepository;
	private UserController userController;
	
	@Autowired
	public PortfolioController(PortfolioRepository portfolioRepository, UserRepository userRepository,
			UserController userController) {
		super();
		this.portfolioRepository = portfolioRepository;
		this.userRepository = userRepository;
		this.userController = userController;
	}

	
	@GetMapping("list/{id}")
	public List<?> listPortfoliosByUserId(@PathVariable (value = "id") long id) {
		User user = userRepository.findUsById(id);
		List<Portfolio> la = portfolioRepository.findByUser(user);
		if (la.size() == 0)
			la = null;	
		return la;
	}
	

	@GetMapping("list")
	public List<Portfolio> listPortfolios() {		
		
		return (List<Portfolio>) portfolioRepository.findAll();
	}
	
	@DeleteMapping("delete/{userId}/{id}")
	public String deletePortfolio(@PathVariable (value = "userId") long userId,@PathVariable("id") long id,HttpServletRequest request ) {
		String jwt = request.getHeader("Authorization").substring(7);
		Long uid = userController.userIdFromToken(jwt);
		if (userId!=uid) throw new RuntimeException("SECURITY : This user is not for the profile owner");
		Portfolio portfolio = portfolioRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid portfolio Id:" + id));
		userRepository.findById(userId).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + userId));
		portfolioRepository.delete(portfolio);
	 return "Portfolio "+id+" has been deleted";	
	}
	
	@PostMapping("add/{userId}")
	public Portfolio addPortfolioToUserById(@Valid @RequestBody Portfolio portfolio, @PathVariable(value = "userId") long userId,HttpServletRequest request ) {
		
		String jwt = request.getHeader("Authorization").substring(7);
		Long uid = userController.userIdFromToken(jwt);
		if (userId!=uid) throw new RuntimeException("SECURITY : This user is not for the profile owner");
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + userId));
		portfolio.setUser(user);
		Portfolio prtf = portfolioRepository.save(portfolio);
		
		user.getPortfolios().add(prtf);
		userRepository.save(user);
		
		return prtf;
	}
	
}
