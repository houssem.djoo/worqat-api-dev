package com.nexym.worqat.controllers;

import java.util.Collections;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Value;
import com.nexym.worqat.dto.TokenDto;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.security.jwt.JwtUtils;

import net.bytebuddy.utility.RandomString;

@Transactional
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/social")
public class SocialController {
	@Value("${google.id}")
	private String idClient;
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	JwtUtils jwtUtils;
	@Autowired
	AuthenticationManager authenticationManager;
	
	@PostMapping("/google")
	public ResponseEntity<User> loginWithGoogle(@RequestBody TokenDto tokendto,HttpServletResponse response) throws Exception{
		NetHttpTransport transport = new NetHttpTransport();
		JacksonFactory factory = JacksonFactory.getDefaultInstance();
		GoogleIdTokenVerifier.Builder ver = new GoogleIdTokenVerifier.Builder(transport,factory)
				.setAudience(Collections.singleton(idClient));
		GoogleIdToken googleIdToken = GoogleIdToken.parse(ver.getJsonFactory(), tokendto.getToken());
		GoogleIdToken.Payload payload = googleIdToken.getPayload();
		
		if (userRepository.existsByEmail(payload.getEmail())) {
			User userAuth = userRepository.findUserByEmail(payload.getEmail());		
			
			String jwt = jwtUtils.generateJwtGoogleSign(userAuth.getEmail());
			response.setHeader("Authorization", jwt);
			userAuth.setPassw(jwt);
			return ResponseEntity.ok().body(userAuth);
			
		}else {
			User newUser = new User();
			newUser.setEmail(payload.getEmail());
			newUser.setFirstName(payload.get("given_name").toString());
			newUser.setLastName(payload.get("family_name").toString());
			newUser.setImageUrl(payload.get("picture").toString());
			newUser.setActive(1);
			newUser.setUsername(newUser.getFirstName()+RandomString.make(3).toLowerCase()+"1");
			while (userRepository.existsByUsername(newUser.getUsername())) {
				newUser.setUsername(newUser.getFirstName()+RandomString.make(3).toLowerCase()+"1");
			}
			String pw = RandomString.make(8);
			newUser.setPassw(pw);
			newUser.setPassword(bCryptPasswordEncoder.encode(pw));
			User result = userRepository.save(newUser);
			Authentication authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(result.getEmail(), result.getPassw()));

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);
			response.setHeader("Authorization", jwt);
			
			return ResponseEntity.ok().body(result);
			
		}
		
	}
}
