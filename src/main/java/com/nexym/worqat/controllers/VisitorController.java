package com.nexym.worqat.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.Banner;
import com.nexym.worqat.entities.Faq;
import com.nexym.worqat.entities.VisitorsNumber;
import com.nexym.worqat.entities.WhyWorqat;
import com.nexym.worqat.repositories.BannerRepository;
import com.nexym.worqat.repositories.FaqRepository;
import com.nexym.worqat.repositories.VisitorsRepository;
import com.nexym.worqat.repositories.WhyWorqatRepository;

@RestController
@RequestMapping("/")
public class VisitorController {
	
	@Autowired
	private VisitorsRepository visitRepository;
	
	@Autowired
	private FaqRepository faqRepository;
	
	@Autowired 
	private BannerRepository bannerRepository;
	
	@Autowired
	private WhyWorqatRepository whyWorqatRepository;
	
	@GetMapping("")
	public void visitorsCount() {
	
		if (faqRepository.findAll().size()==0) {
			Faq faq = new Faq ( "Any sized project", 
					"Flexible payment terms", 
					 "Diverse talent",
					 "Get any job done. From small one-off tasks to large, multi-stage projects.",
					 "Pay your freelancers a fixed price or by the hour. All secured by the Milestone Payments system.",
					 "Choose from expert freelancers in over 1800 skill sets, from all around the globe.", 
					 "1- Post a project or contest", "2- Choose the perfect freelancer", "3- Pay when you’re satisfied",
					 "Simply post a project or contest for what you need done and receive competitive bids from freelancers within minutes.",
					 "Browse freelancer profiles. Chat in real-time. Compare proposals and select the best one. Award your project and your freelancer starts work.",
					 "Pay securely using our Milestone Payment system. Release payments when it has been completed and you're 100% satisfied.",
					 "Any sized project", "Mobile App",
					 "You can live chat with your freelancers to ask questions, share feedback and ge t constant updates on the progress of your work.",
					 "Manage your project at the touch of your fingertips. The mobile app makes on-the-go collaboration a breeze.",
					 "Pay with confidence", "24/7 support", 
					 "Pay safely and securely in over 39 currencies through themilesone payments system.Only relese payement when you are satisfed whith the work",
					 "We're always here to help. Our representatives are avaible 24/7 to assist you with any issues.");
			faqRepository.save(faq);
		}
		
		 /*String remoteAddress = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes())
			       .getRequest().getRemoteAddr();
		 response.setHeader("this-yourIP", remoteAddress);*/
		if (bannerRepository.findAll().size()==0) {
			Banner banner = new Banner ();
			bannerRepository.save(banner);
		}
		
		if (whyWorqatRepository.findAll().size()==0) {
			WhyWorqat whyworqat = new WhyWorqat ("Worqat, an ambitious Qatari technical vision inspired and developed from similar experiences in other countries that seeks to contribute to raising awareness, the level of luxury and the efficiency of spending on services. In addition to creating opportunities for citizens and residents to develop their skills, provide their services and expertise, and benefit from financial and professional returns, which is in line with Qatar's 2030 vision of digital transformation.",
					"Providing the best options for companies and talented people, with transparency, clarity and maximum comfort, while ensuring the lowest costs. By digitizing, developing and embracing the talented and the needs of companies to serve companies, emerging projects and the interests of individuals. Harnessing technologies and artificial intelligence to increase human awareness by creating a clear competitive environment that will contribute to raising the level of awareness in spending efficiency, reducing burdens, and meeting the needs and works of society and the market.",
					"Impartiality and transparency by clarifying legal matters and appropriate alternatives for clients and a commitment to maintain the highest level of standards and ethical practices in accordance with all laws and regulatory frameworks.",
					"Developing the relationship between companies, emerging projects, free talents and individuals. By creating a secure digital platform that displays all companies' requirements for temporary startup prajects, and displays the efficiency of free talents, and compares their prices with clarity and transparency according to the experiences of the talented and the needs of stakeholders.");	
			whyWorqatRepository.save(whyworqat);
		}
		
		if (visitRepository.findAll().size()==0) {
			VisitorsNumber visits = new VisitorsNumber();
			visits.setIpsVisits(null);
			visits.setVistorsNumber(0);
			visitRepository.save(visits);
		}
		VisitorsNumber visits = visitRepository.findAll().get(0);
		visits.setVistorsNumber(visits.getVistorsNumber()+1);
		//visits.setIpsVisits(remoteAddress);
		visitRepository.save(visits);
	
	}
}
