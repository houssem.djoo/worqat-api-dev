package com.nexym.worqat.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.Notification;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.payload.request.SkillsRequired;
import com.nexym.worqat.repositories.NotificationRepository;
import com.nexym.worqat.repositories.ProjectRepository;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.services.DateAttibute;

@CrossOrigin(origins = "*")
@RestController
public class NotificationController {
	@Autowired
	private UserRepository userRepository;
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private ProjectRepository projectRepository;
   
    @PostMapping("/notify/{owner}/{idproject}")
    public String getNotification(@PathVariable(value = "owner") String owner, @PathVariable(value = "idproject") Long idproject, @RequestBody SkillsRequired skills){
    	User projectOwner = userRepository.findUserByUsername(owner);
        List<User> usersList = userRepository.findAll();   
        usersList.remove(projectOwner);
        String required = skills.getText();          	
        usersList.forEach((user)->{
        	user.getSkills().forEach((sk)->{
        		String skillUser = ".*"+sk.getName()+".*";
        		if(required.matches(skillUser)) {
        			Notification notificationa = new Notification();
        			notificationa.setCount(1);
        			notificationa.setName("Check new project require your skills !");
        			notificationa.setDate(DateAttibute.getTime(new Date()).toString());
        			notificationa.setIdProjet(idproject);
        				notificationa.setUser(user);
        				notificationRepository.save(notificationa);
        				template.convertAndSend("/topic/notification/"+user.getUsername(), notificationa);	
        				//userNotif.add(user);
        		}
        	});
        });
        return "Notification sent to users reliable to skills required" ;
    }
    @PostMapping("/notifyaffect/{username}/{idproject}")
    public String sendNotificationOnAffect(@PathVariable(value = "username") String username,@PathVariable(value = "idproject") Long idproject){
    	User user = userRepository.findUserByUsername(username);
        if (user==null) throw new RuntimeException("Enter valid username !");
        Notification notificationa = new Notification();
    	notificationa.setCount(1);
    	notificationa.setName("Congatulation ! You have been accepted on new project.");
    	notificationa.setDate(DateAttibute.getTime(new Date()).toString());
    	notificationa.setIdProjet(idproject);
    			
        				template.convertAndSend("/topic/notification/"+username, notificationa);	
        				notificationa.setUser(user);
        				notificationRepository.save(notificationa);
       
        return "Notification sent to user affected" ;
    }
    
    @PostMapping("/notifysubmit/{idproject}")
    public String sendNotificationOnSubmit(@PathVariable(value = "idproject") Long idproject){
    	
    	User user = projectRepository.findProjectById(idproject).getUser();
        if (user==null) throw new RuntimeException("Enter valid id project !");
        Notification notificationa = new Notification();
    	notificationa.setCount(1);
    	notificationa.setName("Check your submits list you have new freelancer submit to  project #"+idproject+".");
    	notificationa.setDate(DateAttibute.getTime(new Date()).toString());
    	notificationa.setIdProjet(idproject);
        				template.convertAndSend("/topic/notification/"+user.getUsername(), notificationa);	
        				notificationa.setUser(user);
        				notificationRepository.save(notificationa);
       
        return "Notification sent to project holder !" ;
    }
    
    @PostMapping("/notifydelete/{idproject}")
    public String sendNotificationToProjectDeletedOwner(@PathVariable(value = "idproject") Long idproject){
    	
    	User user = projectRepository.findProjectById(idproject).getUser();
        if (user==null) throw new RuntimeException("Enter valid id project !");
        Notification notificationa = new Notification();
    	notificationa.setCount(1);
    	notificationa.setName("Your project #"+idproject+" has been deleted by the admin.");
    	notificationa.setDate(DateAttibute.getTime(new Date()).toString());
    	notificationa.setIdProjet(idproject);
        				template.convertAndSend("/topic/notification/"+user.getUsername(), notificationa);	
        				notificationa.setUser(user);
        				notificationRepository.save(notificationa);
       
        return "Notification sent to project holder !" ;
    }
    @PostMapping("/notifyreview/{iduser}")
    public String sendNotificationToReviewedUser(@PathVariable(value = "iduser") Long iduser){
    	
    	User user =  userRepository.findUsById(iduser);
        if (user==null) throw new RuntimeException("Enter valid user id !");
        Notification notificationa = new Notification();
    	notificationa.setCount(1);
    	notificationa.setName("You have new review added in your profile !");
    	notificationa.setDate(DateAttibute.getTime(new Date()).toString());
    			
        				template.convertAndSend("/topic/notification/"+user.getUsername(), notificationa);	
        				notificationa.setUser(user);
        				notificationRepository.save(notificationa);
       
        return "Notification sent to project holder !" ;
    }
    
    
    
}