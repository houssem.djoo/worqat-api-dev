package com.nexym.worqat.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.Project;
import com.nexym.worqat.entities.Ticket;
import com.nexym.worqat.repositories.ProjectRepository;
import com.nexym.worqat.repositories.TicketRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/search")
public class SearchController {
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired 
	private TicketRepository ticketRepository;
		
	@GetMapping("/{search}")
	public List<Project> searchProjects(@PathVariable (value = "search") String search ) throws Exception{
		String sch = search.toLowerCase();
		List<Project> projects = projectRepository.findAll();
		List<Project> projectsResult = new ArrayList<>();
		projects.forEach((pr)->{
			if (pr.getTitle().toLowerCase().contains(sch) || pr.getCategory().toLowerCase().contains(sch) 
					|| pr.getSkillsReq().toString().toLowerCase().contains(sch) ||
					Float.toString(pr.getPrice()).contains(search)
					)  {
				pr.setFreelancerAffected(null);
				pr.setFreelancersSubmits(null);
				projectsResult.add(pr);
		}
			});
		return projectsResult;
	}
	
	 @GetMapping("/projects/{status}")
	    public List<Project> getSearchProjects(@PathVariable(value = "status") String status) {
		
		 if (status=="pending" || status=="working on" || status=="in despute" || status=="closed") ;
		 return projectRepository.findProjectByStatus(status);
		
		
	 }
	 
	 @GetMapping("/tickets/{status}")
	    public List<Ticket> getSearchTickets(@PathVariable(value = "status") String status) {
		
		 if (status=="open" || status=="pending" || status=="resolved" || status=="closed") ;
		 return ticketRepository.findTicketByStatus(status);
		
		
	 }
}
