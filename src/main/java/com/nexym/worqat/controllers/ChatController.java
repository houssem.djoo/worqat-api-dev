package com.nexym.worqat.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import com.nexym.worqat.entities.Conversation;
import com.nexym.worqat.entities.Message;
import com.nexym.worqat.repositories.ConversationRepository;
import com.nexym.worqat.repositories.MessageRepository;
import com.nexym.worqat.services.DateAttibute;
import com.pusher.rest.Pusher;

@CrossOrigin(origins = "*")
@RestController
public class ChatController {
	@Autowired
	private MessageRepository messageRepository;

	@Autowired
	private ConversationRepository conversationRepository;
	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	@MessageMapping("/chat/{conversationName}")
	public void sendMessage(@DestinationVariable String conversationName, Message message) {

		Date Datedata = DateAttibute.getTime(new Date());
		Conversation conv = conversationRepository.findConversationByConversationName(conversationName);
		message.setCreatedAt(Datedata.toString());
		message.setConversation(conv);
		conv.setLastMsg(Datedata);
		messageRepository.save(message);
		conversationRepository.save(conv);
		simpMessagingTemplate.convertAndSend("/topic/messages/" + conversationName, message);

	}
	/*@MessageMapping("/notification/{username}")
	public void senNotification(@DestinationVariable String username, Notification notification) {

		Date Datedata = DateAttibute.getTime(new Date());
		notification.setCreatedAt(Datedata.toString());
		notification.setConversation();
		messageRepository.save(notification);
		
		simpMessagingTemplate.convertAndSend("/subject/notifications/" + username, notification);

	}*/
	/*
	 * @PostMapping("/messages/{conversationName}") public Message
	 * sendMessage(@PathVariable (value = "conversationName") String
	 * conversationName ,@RequestBody Message message) {
	 * 
	 * Pusher pusher = new Pusher("1305949", "b6102ef583638890f87b",
	 * "bc121d3c679e170fcbe0"); pusher.setCluster("eu"); pusher.setEncrypted(true);
	 * 
	 * pusher.trigger(conversationName, "message", message);
	 * 
	 * 
	 * Date Datedata = DateAttibute.getTime(new Date());
	 * message.setCreatedAt(Datedata.toString());
	 * message.setConversation(conversationRepository.
	 * findConversationByConversationName(conversationName));
	 * messageRepository.save(message);
	 * 
	 * return message; }
	 */

	@GetMapping("/msg/{sender}/{reciever}")
	public List<Message> getMessages(@PathVariable(value = "sender") String sender,
			@PathVariable(value = "reciever") String reciever) {
		List<Message> msgList = messageRepository.findByReciever(reciever);
		List<Message> messages = new ArrayList<>();
		msgList.forEach((lst) -> {
			if (lst.getSender().contains(sender) || lst.getReciever().contains(sender)) {
				messages.add(lst);
			}

		});

		return messages;

	}
}