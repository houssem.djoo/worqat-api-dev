package com.nexym.worqat.controllers;

import java.util.Date;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.Project;
import com.nexym.worqat.entities.SubmitsRequest;
import com.nexym.worqat.entities.Ticket;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.repositories.ProjectRepository;
import com.nexym.worqat.repositories.SubmitsRepository;
import com.nexym.worqat.repositories.TicketRepository;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.services.DateAttibute;


@RestController
@RequestMapping("admin")
public class AdminController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TicketRepository ticketRepository;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired 
	private SubmitsRepository submitsRepository;
	
	/**************** Users Administration **********************/
	
	@PostMapping("/ban3/{userId}")
	public String banUserThreeDays(@PathVariable(value = "userId") long userId) {
		User userBan = userRepository.findUsById(userId);
		userBan.setBanned(true);
		userBan.setBannedUntil(DateAttibute.banThreeDays(new Date()));
		userRepository.save(userBan);
	return "User has been banned for 3 days";
	}
	
	@PostMapping("/ban7/{userId}")
	public String banUserSevenDays(@PathVariable(value = "userId") long userId) {
		User userBan = userRepository.findUsById(userId);
		userBan.setBanned(true);
		userBan.setBannedUntil(DateAttibute.banThreeDays(new Date()));
		userRepository.save(userBan);
	return "User has been banned for 7 days";
	}
	
	@PostMapping("/ban15/{userId}")
	public String banUserFifteenDays(@PathVariable(value = "userId") long userId) {
		User userBan = userRepository.findUsById(userId);
		userBan.setBanned(true);
		userBan.setBannedUntil(DateAttibute.banFifteenDays(new Date()));
		userRepository.save(userBan);
	return "User has been banned for 3 days";
	}
	
	@PostMapping("/ban30/{userId}")
	public String banUserThirtyDays(@PathVariable(value = "userId") long userId) {
		User userBan = userRepository.findUsById(userId);
		userBan.setBanned(true);
		userBan.setBannedUntil(DateAttibute.banThirtyDays(new Date()));
		userRepository.save(userBan);
	return "User has been banned for 3 days";
	}
	
	@PostMapping("/banpermantly/{userId}")
	public String banUserPermantly(@PathVariable(value = "userId") long userId) {
		User userBan = userRepository.findUsById(userId);
		userBan.setBanned(true);
		userBan.setBannedUntil(DateAttibute.banPermantly(new Date()));
		userRepository.save(userBan);
	return "User has been banned permantly";
	}
	
	@PostMapping("/unban/{userId}")
	public String unBanUser(@PathVariable(value = "userId") long userId) {
		User userunBan = userRepository.findUsById(userId);
		userunBan.setBanned(false);
		userunBan.setBannedUntil(null);
		userRepository.save(userunBan);
	return "User has been unbanned";
	}
	
	/**************** Tickets Administration **********************/
	
	@PutMapping("/ticket/update/{ticketId}")
	public Ticket changeStatus(@PathVariable(value = "ticketId")long ticketId, @RequestBody String status) {
		Ticket ticket = ticketRepository.findById(ticketId).get();
		
		if (status.contains("pending")) {
			ticket.setStatus("pending");
		}else if (status.contains("resolved")) {
			ticket.setStatus("resolved");
		}else if (status.contains("closed")) {
			ticket.setStatus("closed");
		}else if (status.contains("open")){
			ticket.setStatus("open");
		}else throw new RuntimeException("Enter valid status (pending, resolved, closed)");
			
		
		return ticketRepository.save(ticket);
	}
	
	/**************** Projects Administration **********************/
	
	@PutMapping("/update/{projectId}")
	Project updateProject(@PathVariable(value = "projectId") long projectId, @RequestBody String freelancer) throws Exception {
		Project proj = projectRepository.findProjectById(projectId);
		proj.setFreelancerAffected(freelancer);
		return projectRepository.save(proj);
	}
	
	@PostMapping("/remove/{projectId}")
	Project removeFreelancer(@PathVariable(value = "projectId") Long projectId,HttpServletRequest request) throws Exception{
		Project project = projectRepository.findProjectById(projectId);
		
		if(project!=null) {
		project.setFreelancerAffected(null);
		project.setStatus("pending");
		
		return projectRepository.save(project);
		
		} else throw new RuntimeException("There is no freelancer affected on this project");
	}
	
	@DeleteMapping("/project/delete/{id}")
	public String deleteProject(@PathVariable("id") long id) {
		Project project = projectRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid project Id:" + id));
		Set<SubmitsRequest> subList = project.getFreelancersSubmits();	
					project.removeAllSubmits();
					subList.forEach((sb)->{		
						submitsRepository.delete(sb);
					});
					project.removeAllSkills();
					projectRepository.save(project);				
		projectRepository.delete(project);
	 return "Project "+id+" has been deleted";	
	}
	
}
