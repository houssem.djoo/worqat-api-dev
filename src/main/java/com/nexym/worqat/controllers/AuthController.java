package com.nexym.worqat.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import com.nexym.worqat.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.payload.request.LoginRequest;
import com.nexym.worqat.repositories.RoleRepository;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.security.jwt.JwtUtils;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	private UserService userService;

	@Transactional
	@PostMapping("/signin")
	public User authenticateUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletResponse response)
			throws Exception {
		String userEmail;
		User userByUsername = userRepository.findUserByUsername(loginRequest.getEmail().toLowerCase());
		User userByEmail = userRepository.findUserByEmail(loginRequest.getEmail().toLowerCase());
		if (userByUsername != null) {
			userEmail = userByUsername.getEmail().toLowerCase();
		} else if (userByEmail != null) {
			userEmail = userByEmail.getEmail().toLowerCase();
		} else
			throw new RuntimeException("User doesn't exist ! Enter Valid email or username");
		User userAuth = userService.findUserByEmail(userEmail);
		if (userAuth.getActive() == 0)
			throw new RuntimeException("User is not Activated");
		if (userAuth.getBanned() == true) {
			Boolean banned = userAuth.getBannedUntil().after(new Date());
			if (banned == false) {
				userAuth.setBanned(false);
				userAuth.setBannedUntil(null);
				userRepository.save(userAuth);
			} else
				throw new RuntimeException("You are banned until " + userAuth.getBannedUntil().toString());

		} else if (userAuth.getBanned() == false)
			;
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(userEmail, loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		response.setHeader("Authorization", jwt);
		Boolean status = authentication.isAuthenticated();
		if (status == true) {
			userAuth.setOnlineStatus("online");
		} else
			userAuth.setOnlineStatus("offline");
		userRepository.save(userAuth);
		return userService.findUserByEmail(userEmail);
	}

	@Transactional
	@PostMapping("/signup")
	public User createUser(@Valid @RequestBody User user) throws MessagingException, IOException, Exception {
		User userExists = userService.findUserByEmail(user.getEmail().toLowerCase());
		if (!user.getPasswordConfirmation().equals(user.getPassword()))
			throw new RuntimeException("Password doesn't match");
		if (userService.findUserByUsername(user.getUsername().toLowerCase()) != null)
			throw new RuntimeException("Username already exist !");
		if (userExists != null) {
			throw new RuntimeException("User exist !");
		} else {
			user.setEmail(user.getEmail().toLowerCase());
			user.setUsername(user.getUsername().toLowerCase());
			userService.saveUser(user);
			String message = "Hello, Can you activate your account with the following url "
					+ "You can log in : https://worqat-api.herokuapp.com/accounts/enable?confirmation="
					+ user.getConfirmationToken() + " \n Best Regards!";
			sendmail(user.getEmail(), message, "Activation account");
		}
		return userRepository.save(user);
	}

	public void sendmail(String to, String message, String subject)
			throws MessagingException, IOException, javax.mail.MessagingException {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication("worqatfreelancers@gmail.com", "Work123456789");
			}
		});
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(to, false));
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
		msg.setSubject(subject);
		msg.setSentDate(new Date());
		msg.setText(message);
		Transport.send(msg);
	}
}
