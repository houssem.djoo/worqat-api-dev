package com.nexym.worqat.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.Skill;
import com.nexym.worqat.repositories.SkillRepository;


	@RestController
	@RequestMapping("/skills")
	@CrossOrigin(origins = "*")
	public class SkillController {
		@Autowired
		private SkillRepository skillRepository;
	
		@GetMapping("list")
		public List<Skill> listSkills() {
			return skillRepository.findAll();
		}

		@PostMapping("add")
		public String addSkill(@RequestParam("skillName") String skill) throws Exception{
		
			if (skill==null )throw new RuntimeException("Enter new skill !"); 
			System.out.println(skill);
			Skill sk = new Skill(skill);
			Skill newSkill = skillRepository.save(sk);
			System.out.println("Skill = " + newSkill);
			return "Skill "+newSkill+" added ";
		}
		@DeleteMapping("/delete/{skillName}")
		public ResponseEntity<?> deleteSkill(@PathVariable String skillName) {
			
			 Skill skill = skillRepository.findByName(skillName);		
			 skillRepository.delete(skill);		
			return ResponseEntity.ok().body("Skill "+skillName+" has been deleted");
		
		}
}
