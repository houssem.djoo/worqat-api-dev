package com.nexym.worqat.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.nexym.worqat.entities.Ticket;
import com.nexym.worqat.repositories.TicketRepository;

@RestController
@RequestMapping("/ticket")
@CrossOrigin(origins = "*")
public class TicketController {
	
	@Autowired
	private TicketRepository ticketRepository;
	
	@GetMapping("list")
	public List<Ticket> getAllTickets(){
		
		return ticketRepository.findAll();
	}
	
	@GetMapping("/{ticketId}")
	public Ticket getTicket(@PathVariable(value = "ticketId") long ticketId){
		
		return ticketRepository.findById(ticketId).get();
	}
	@PostMapping("add")
	public Ticket addTicket(@RequestBody Ticket ticketRequest) {
		ticketRequest.setStatus("open");
		Ticket newticket = ticketRepository.save(ticketRequest);
		return newticket ;
	}

}
