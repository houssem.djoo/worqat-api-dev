package com.nexym.worqat.controllers;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nexym.worqat.entities.User;
import com.nexym.worqat.payload.request.ForgotPasswordRequest;
import com.nexym.worqat.payload.request.NewPasswordSend;
import com.nexym.worqat.services.UserNotFoundException;
import com.nexym.worqat.services.UserService;

import net.bytebuddy.utility.RandomString;

@RestController
@RequestMapping({ "/reset" })
@CrossOrigin(origins = "*")
public class ForgotPasswordController {
	
    @Autowired
    private AuthController authController;
     
    @Autowired
    private UserService userService;
    
    @PostMapping("/forgot_password")
    public String processForgotPassword(@RequestBody ForgotPasswordRequest forgotpasswordrequest, HttpServletResponse response) throws MessagingException, IOException {
    	   
    		String email = forgotpasswordrequest.getEmail();

    	    String token = RandomString.make(30);
    	    
    	    try {
    	        userService.updateResetPasswordToken(token, email);
    	        String resetPasswordLink ="https://worqat-api.herokuapp.com/resetpw/reset_password_link?token=" + token;
    	        authController.sendmail(email, resetPasswordLink,"Reset password");
    	      //  model.addAttribute("message", "We have sent a reset password link to your email. Please check.");
    	         
    	    } catch (UserNotFoundException ex) {
    	        	return ex.getMessage();
    	    }   
    	    return "succes send mail";
    }
    /*
    @GetMapping("/resetlink")
    public void resetLinkAccess(@Param(value = "token") HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String token = request.getParameter("token");
    	User user=userService.getByResetPasswordToken(token);
    	response.setHeader("tokenReset", token);
    	String resetPasswordUrl = "http://worqat-api.herokuapp.com/reset/reset_password?token="+token;
    	if (user!=null) {
    		
    		response.sendRedirect(resetPasswordUrl);
    		request.setAttribute("token", token);
    }
    	else {throw new RuntimeException("Bad token to reset password");}
    	
    }
     
    
    @PostMapping("/reset_password")
    public User processResetPassword(@RequestParam(value = "token") String token,@RequestBody NewPasswordSend passwordRequest) throws Exception{
 	   User user=userService.getByResetPasswordToken(token);
 	   if (user==null) throw new RuntimeException("Bad token");
 	   if (passwordRequest.getPassword().equals(passwordRequest.getPasswordConfirmation())) {
    	userService.updatePassword(user, passwordRequest.getPassword());
    	return user;
 	   }else throw new RuntimeException("Password doesn't match");
 	  
 
    }*/

}
