package com.nexym.worqat.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nexym.worqat.entities.Role;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.payload.request.NewPasswordSend;
import com.nexym.worqat.services.UserService;

@Controller
@RequestMapping({ "/resetpw" })
public class NewPasswordController {
	     
	    @Autowired
	    private UserService userService;
	    
	  
	    @GetMapping("/reset_password_link")
	    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
	    	User user=userService.getByResetPasswordToken(token);
	        model.addAttribute("token", token);
	         
	        if (user == null) {
	            model.addAttribute("message", "Invalid Token");
	            return "message";
	        }
	         
	        return "reset/resetpass";
	    }
	  /*  @PostMapping("/reset_password")
	    public User processResetPassword(@Param(value = "token") String token,@RequestBody NewPasswordSend passwordRequest) throws Exception{
	 	   User user=userService.getByResetPasswordToken(token);
	 	   if (user==null) throw new RuntimeException("Bad token");
	 	   if (passwordRequest.getPassword().equals(passwordRequest.getPasswordConfirmation())) {
	    	userService.updatePassword(user, passwordRequest.getPassword());
	    	return user;
	 	   }else throw new RuntimeException("Password doesn't match");
	 	  
	 
	    }
	    */
	    /*
	    @PostMapping("/reset_password")
	    public String processResetPassword(@RequestParam HttpServletRequest request, Model model) {
	        String token = request.getParameter("token");
	        String password = request.getParameter("password");
	         
	        User user=userService.getByResetPasswordToken(token);
	        model.addAttribute("title", "Reset your password");
	         
	        if (user != null) {
	        	userService.updatePassword(user, password);
	            
	            return "You have successfully changed your password.";
	        } else {           
	        	model.addAttribute("message", "Invalid Token");
	            return"Bad req"; 
	            //model.addAttribute("message", "You have successfully changed your password.");
	        }
	         
	       // return "role/listRoles";
	    }
*/
	    
	    @PostMapping("reset_password")
	    @ResponseBody
		public String addRole(@RequestParam("token") String token,@RequestParam("password") String password,@RequestParam("passwordConfirm") String passwordConfirm) throws Exception{
	    	User user=userService.getByResetPasswordToken(token);
	    	   if (user==null) throw new RuntimeException("Bad token");
	    	   if (password.equals(passwordConfirm)) {
	       	userService.updatePassword(user, password);
	       	return "Hello guys";
	    	   }else throw new RuntimeException("Password doesn't match");
		}

}
