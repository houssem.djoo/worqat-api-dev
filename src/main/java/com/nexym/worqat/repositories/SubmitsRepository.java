package com.nexym.worqat.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.SubmitsRequest;
import com.nexym.worqat.entities.User;

@Repository
public interface SubmitsRepository extends JpaRepository<SubmitsRequest, Long>{
	List<SubmitsRequest> findByUser (User user);
	
}
