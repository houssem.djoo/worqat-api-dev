package com.nexym.worqat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Skill;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long>{

	 Skill findByName (String skill);

}