package com.nexym.worqat.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Conversation;
@Repository
public interface ConversationRepository extends JpaRepository<Conversation, Long> {
		Conversation findConversationById (Long idConv);
		Conversation findConversationByConversationName (String convName);
	}

