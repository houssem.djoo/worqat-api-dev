package com.nexym.worqat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Faq;

@Repository
public interface FaqRepository extends JpaRepository<Faq, Long> {
	Faq findFaqById(Long id);
}
