package com.nexym.worqat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Historic;

@Repository
public interface HistoricRepository extends JpaRepository<Historic, Long> {
	
	Historic findHistByYear (Integer year);
	Historic findHistByYearAndProjects (Integer year,String projects);

}
