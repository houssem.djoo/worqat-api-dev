package com.nexym.worqat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.VisitorsNumber;

@Repository
public interface VisitorsRepository extends JpaRepository<VisitorsNumber, Long>{

}
