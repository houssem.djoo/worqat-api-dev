package com.nexym.worqat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Notification;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

}
