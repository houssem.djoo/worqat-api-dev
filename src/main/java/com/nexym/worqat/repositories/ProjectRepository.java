package com.nexym.worqat.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.nexym.worqat.entities.Project;
import com.nexym.worqat.entities.User;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
	List<Project> findByUser (User user);
	Project findProjectById (Long id);
	List<Project> findProjectByTitle(String name);
	List<Project> findProjectByCategory(String name);
	List<Project> findProjectByPrice(float price);
	List<Project> findProjectByStatus(String status);
	List<Project> findProjectByFreelancerAffected(String username);
	
}
