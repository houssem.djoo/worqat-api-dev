package com.nexym.worqat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Banner;

@Repository
public interface BannerRepository extends JpaRepository<Banner, Long>{

}
