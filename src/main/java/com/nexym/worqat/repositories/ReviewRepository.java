package com.nexym.worqat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {

}
