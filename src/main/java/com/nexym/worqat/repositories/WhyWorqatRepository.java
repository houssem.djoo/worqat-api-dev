package com.nexym.worqat.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.WhyWorqat;

@Repository
public interface WhyWorqatRepository extends JpaRepository<WhyWorqat, Long>{
	WhyWorqat findWhyById (Long id);
}
