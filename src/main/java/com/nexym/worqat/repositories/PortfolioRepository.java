package com.nexym.worqat.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Portfolio;
import com.nexym.worqat.entities.User;

@Repository
public interface PortfolioRepository extends CrudRepository<Portfolio, Long>{
	Portfolio findByTitle (String title);
	List<Portfolio> findByUser (User user);
}
