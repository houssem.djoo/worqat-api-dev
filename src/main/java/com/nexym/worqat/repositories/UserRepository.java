package com.nexym.worqat.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Role;
import com.nexym.worqat.entities.User;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	@Query(
			value = "Select * from users Order by rating desc,nb_projects_worked desc LIMIT 5",
			nativeQuery = true)
	List<User> findTopUsers();
	Optional<User> findByUsername(String username);

	Optional<User> findByEmail(String email);

	Boolean existsByUsername(String username);

	Boolean existsByPassword(String password);

	Boolean existsByEmail(String email);

	Optional<User> findUserById(Long id);
	
	User findUsById(Long id);
	
	List<User> findUserByRolesId(Integer id);
	
	List<User> findUserByRoles(Role role);
	
	public User findUserByConfirmationToken (String confirmationToken);
	
	public User findUserByEmail(String email);
	
	public User findByResetPasswordToken(String token);

	public User findUserByUsername(String username);
	
	//List<User>  findUserBySkillName(String skill);


//	@Transactional
//	public User validateUser(String Username, String password) {
//		return User.validateUser(Username, password);
//	}}

}
