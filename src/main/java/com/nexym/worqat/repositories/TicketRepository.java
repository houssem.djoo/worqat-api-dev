package com.nexym.worqat.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexym.worqat.entities.Project;
import com.nexym.worqat.entities.Ticket;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long>{
	List<Ticket> findTicketByStatus(String status);
}
