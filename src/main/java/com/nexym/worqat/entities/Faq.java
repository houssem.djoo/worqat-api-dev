package com.nexym.worqat.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "faq")
public class Faq {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	//@NotBlank(message = "title1 name is mandatory")
	@Column(name = "title1")
	private String title1;
	
	//@NotBlank(message = "title2 is mandatory")
	@Column(name = "title2")
	private String title2;
	
	//@NotBlank(message = "title3 is mandatory")
	@Column(name = "title3")
	private String title3;
	
	//@NotBlank(message = "Description1 is mandatory")
	@Column(name = "Description1", columnDefinition="TEXT") 
	private String description1;
	
	//@NotBlank(message = "Description2 is mandatory")
	@Column(name = "Description2", columnDefinition="TEXT") 
	private String description2;
	
	//@NotBlank(message = "Description3 is mandatory")
	@Column(name = "Description3", columnDefinition="TEXT") 
	private String description3;
	
	//@NotBlank(message = "title4 name is mandatory")
	@Column(name = "title4")
	private String title4;
	
	//@NotBlank(message = "title5 is mandatory")
	@Column(name = "title5")
	private String title5;
	
	//@NotBlank(message = "title6 is mandatory")
	@Column(name = "title6")
	private String title6;
	
	//@NotBlank(message = "Description4 is mandatory")
	@Column(name = "Description4", columnDefinition="TEXT") 
	private String description4;
	
	//@NotBlank(message = "Description5 is mandatory")
	@Column(name = "Description5", columnDefinition="TEXT") 
	private String description5;
	
	//@NotBlank(message = "Description6 is mandatory")
	@Column(name = "Description6", columnDefinition="TEXT") 
	private String description6;
	
	//@NotBlank(message = "title7 is mandatory")
	@Column(name = "title7")
	private String title7;
	
	//@NotBlank(message = "title8 is mandatory")
	@Column(name = "title8")
	private String title8;
	
	//@NotBlank(message = "Description7 is mandatory")
	@Column(name = "Description7", columnDefinition="TEXT") 
	private String description7;
	
	//@NotBlank(message = "Description8 is mandatory")
	@Column(name = "Description8", columnDefinition="TEXT") 
	private String description8;
	
	//@NotBlank(message = "title9 is mandatory")
	@Column(name = "title9")
	private String title9;
	
	//@NotBlank(message = "title10 is mandatory")
	@Column(name = "title10")
	private String title10;
	
	//@NotBlank(message = "Description9 is mandatory")
	@Column(name = "Description9", columnDefinition="TEXT") 
	private String description9;
	
	//@NotBlank(message = "Description10 is mandatory")
	@Column(name = "Description10", columnDefinition="TEXT") 
	private String description10;

	public Faq(String title1, String title2, String title3, String description1, String description2,
			String description3, String title4, String title5, String title6, String description4, String description5,
			String description6, String title7, String title8, String description7, String description8, String title9,
			String title10, String description9, String description10) {
		super();
		
		this.title1 = title1;
		this.title2 = title2;
		this.title3 = title3;
		this.description1 = description1;
		this.description2 = description2;
		this.description3 = description3;
		this.title4 = title4;
		this.title5 = title5;
		this.title6 = title6;
		this.description4 = description4;
		this.description5 = description5;
		this.description6 = description6;
		this.title7 = title7;
		this.title8 = title8;
		this.description7 = description7;
		this.description8 = description8;
		this.title9 = title9;
		this.title10 = title10;
		this.description9 = description9;
		this.description10 = description10;
	}

	public Faq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle1() {
		return title1;
	}

	public void setTitle1(String title1) {
		this.title1 = title1;
	}

	public String getTitle2() {
		return title2;
	}

	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	public String getTitle3() {
		return title3;
	}

	public void setTitle3(String title3) {
		this.title3 = title3;
	}

	public String getDescription1() {
		return description1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public String getDescription3() {
		return description3;
	}

	public void setDescription3(String description3) {
		this.description3 = description3;
	}

	public String getTitle4() {
		return title4;
	}

	public void setTitle4(String title4) {
		this.title4 = title4;
	}

	public String getTitle5() {
		return title5;
	}

	public void setTitle5(String title5) {
		this.title5 = title5;
	}

	public String getTitle6() {
		return title6;
	}

	public void setTitle6(String title6) {
		this.title6 = title6;
	}

	public String getDescription4() {
		return description4;
	}

	public void setDescription4(String description4) {
		this.description4 = description4;
	}

	public String getDescription5() {
		return description5;
	}

	public void setDescription5(String description5) {
		this.description5 = description5;
	}

	public String getDescription6() {
		return description6;
	}

	public void setDescription6(String description6) {
		this.description6 = description6;
	}

	public String getTitle7() {
		return title7;
	}

	public void setTitle7(String title7) {
		this.title7 = title7;
	}

	public String getTitle8() {
		return title8;
	}

	public void setTitle8(String title8) {
		this.title8 = title8;
	}

	public String getDescription7() {
		return description7;
	}

	public void setDescription7(String description7) {
		this.description7 = description7;
	}

	public String getDescription8() {
		return description8;
	}

	public void setDescription8(String description8) {
		this.description8 = description8;
	}

	public String getTitle9() {
		return title9;
	}

	public void setTitle9(String title9) {
		this.title9 = title9;
	}

	public String getTitle10() {
		return title10;
	}

	public void setTitle10(String title10) {
		this.title10 = title10;
	}

	public String getDescription9() {
		return description9;
	}

	public void setDescription9(String description9) {
		this.description9 = description9;
	}

	public String getDescription10() {
		return description10;
	}

	public void setDescription10(String description10) {
		this.description10 = description10;
	}
	
}