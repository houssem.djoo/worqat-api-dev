package com.nexym.worqat.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "comment")
public class Comment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotBlank(message = "commentator name is mandatory")
	@Column(name = "comment_owner")
	private String commentOwner;
	
	@NotBlank(message = "comment is mandatory")
	@Column(columnDefinition="TEXT")
	private String content;
	
	@JsonIgnore
	@NotNull
	@ManyToOne
    @JoinColumn(name = "review_id", referencedColumnName = "review_id")
	private Review review;
	
	@Column(name = "date_comment")
	private String createdAt;
	
	public Comment(long id, @NotBlank(message = "commentator name is mandatory") String commentOwner,
			@NotBlank(message = "comment is mandatory") String content, @NotNull Review review, String createdAt) {
		super();
		this.id = id;
		this.commentOwner = commentOwner;
		this.content = content;
		this.review = review;
		this.createdAt = createdAt;
	}

	public Comment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCommentOwner() {
		return commentOwner;
	}

	public void setCommentOwner(String commentOwner) {
		this.commentOwner = commentOwner;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		this.review = review;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	
	
}