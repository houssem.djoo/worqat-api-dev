package com.nexym.worqat.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "skills", uniqueConstraints = { @UniqueConstraint(columnNames = "skill_name") })
public class Skill {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "skill_id")
	private long id;

	@Column(name = "skill_name")
	@NotEmpty(message = "*Please provide your skill name")
	private String name;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "skills")
	private Set<User> users = new HashSet<>();
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "skillsReq")
	private Set<Project> projects = new HashSet<>();
	
	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public Skill() {

	}

	public Skill(String name) {
		super();
		this.name = name;
	}

	public String toString() {
		return this.name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	
}

	/*
	 * @JsonIgnore
	 * 
	 * @ManyToMany private Set<User> users = new HashSet<>();
	 * 
	 * public Set<User> getUsers() { return users; }
	 */
	/*
	 * @JsonIgnore
	 * 
	 * @ManyToMany(mappedBy = "skills") private Set<User> users;
	 * 
	 */

	/*
	 * private User user;
	 * 
	 * public User getUser() { return user; }
	 * 
	 * public void setUser(User user) { this.user = user; }
	 */
/*
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}**/
