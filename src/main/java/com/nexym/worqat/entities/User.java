package com.nexym.worqat.entities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.bind.annotation.DeleteMapping;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name = "users", uniqueConstraints = {
				@UniqueConstraint(columnNames = "username"),
				@UniqueConstraint(columnNames = "email")
})

public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private long id;

	@Column(name = "email")
	@Email(message = "*Please provide a valid Email")
	@NotEmpty(message = "*Please provide an email")
	private String email;

	@Column(name = "password")
	//@Length(min = 8, message = "*Your password must have at least 8 characters")
	//@NotEmpty(message = "*Please provide your password")
	private String password;
	
	@Column(name = "password_confirmation")
	//@Length(min = 8, message = "*Your password must have at least 8 characters")
	//@NotEmpty(message = "*Please provide your password confirmation")
	private String passwordConfirmation;
	
	@Column(name = "first_name")
	//@NotEmpty(message = "*Please provide your firstname")
	private String firstName;
	
	@Column(name = "last_name")
	//@NotEmpty(message = "*Please provide your last name")
	private String lastName;
	
	@Column(name = "username")
	//@NotEmpty(message = "*Please provide your username")
	private String username;
	
	@Column(name = "passw")
	//@NotEmpty(message = "*Please provide your username")
	private String passw;
	

	@Column(name = "active")
	private int active;

	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "users_skills",
	joinColumns = { @JoinColumn(name = "user_id")},
	inverseJoinColumns = { @JoinColumn (name = "skill_id")})
	private Set<Skill> skills = new HashSet<>();
	
	public Set<Skill> getSkills() {
		return skills;
	}

	public void setSkills(Set<Skill> skills) {
		this.skills = skills;
	}

	@OneToMany(mappedBy = "user")
	@Column(name = "portfolios")
	private Set<Portfolio> portfolios = new HashSet<>();
	
	@JsonIgnore
	@OneToMany(mappedBy = "user")
	@Column(name = "projects")
	private Set<Project> projects = new HashSet<>();
	
	@JsonIgnore
	@OneToMany(mappedBy = "user")
	@Column(name="submits_projects")
	private Set<SubmitsRequest> submitsUser = new HashSet<>();
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "conversations_users",
	joinColumns = { @JoinColumn(name = "user_id")},
	inverseJoinColumns = { @JoinColumn (name = "conversation_id")})
	private Set<Conversation> conversations = new HashSet<>();
	
	@OneToMany(mappedBy = "user")
	@Column(name = "reviews")
	private Set<Review> reviews = new HashSet<>();
	
	@OneToMany(mappedBy = "user")
	@Column(name = "notifications")
	private Set<Notification> notifications = new HashSet<>();

	@Column
	private int nbProjectsWorked = 0;
	
	@Column
	private Boolean banned = false;
	
	@Column
	private Date bannedUntil;
	
	@Column(name = "phone_number")
	//@NotEmpty(message = "*Please provide your phone number")
	private String phoneNumber;

	@Column(name = "nationality")
	//@NotEmpty(message = "*Please provide your nationatlity")
	private String nationality;
	
	@Column(name = "gender")
	//@NotEmpty(message = "*Please provide your gender")
	private String gender;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "languages")
	private String languages;
	
	@Column(name = "educations")
	private String education;

	@Column(name = "image_url")
	private String imageUrl;
	
	@Column(name = "created_At")
	private String createdAt;
	
	@Column(name = "online_status")
	private String onlineStatus;
	
	@Column(name = "rating")
	private Double rating = 1.0;
	
	@Column(name = "price_per_hour")
	private Double pricePerHour;
	
	@Column(name = "reset_password_token")
	private String resetPasswordToken;
	
	@Column(name = "confirmation_token")
	private String confirmationToken;
	
	public String getPassw() {
		return passw;
	}

	public void setPassw(String passw) {
		this.passw = passw;
	}

	public int getNbProjectsWorked() {
		return nbProjectsWorked;
	}

	public void setNbProjectsWorked(int nbProjectsWorked) {
		this.nbProjectsWorked = nbProjectsWorked;
	}

	public Set<Review> getReviews() {
		return reviews;
	}

	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	public Set<Conversation> getConversations() {
		return conversations;
	}

	public void setConversations(Set<Conversation> conversations) {
		this.conversations = conversations;
	}
	
	public Date getBannedUntil() {
		return bannedUntil;
	}

	public void setBannedUntil(Date bannedUntil) {
		this.bannedUntil = bannedUntil;
	}

	public Boolean getBanned() {
		return banned;
	}

	public void setBanned(Boolean banned) {
		this.banned = banned;
	}

	public Set<SubmitsRequest> getSubmitsUser() {
		return submitsUser;
	}

	public void setSubmitsUser(Set<SubmitsRequest> submitsUser) {
		this.submitsUser = submitsUser;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public Set<Portfolio> getPortfolios() {
		return portfolios;
	}

	public void setPortfolios(Set<Portfolio> portfolios) {
		this.portfolios = portfolios;
	}
	
	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(String onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Double getPricePerHour() {
		return pricePerHour;
	}

	public void setPricePerHour(Double pricePerHour) {
		this.pricePerHour = pricePerHour;
	}

	@JsonIgnore
	public String getConfirmationToken() {
		return confirmationToken;
	}

	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}

	@JsonIgnore
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}
	@JsonSetter
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
	@JsonIgnore
	public String getResetPasswordToken() {
		return resetPasswordToken;
	}
	
	public void setResetPasswordToken(String resetPasswordToken) {
		this.resetPasswordToken = resetPasswordToken;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@JsonIgnore
	public String getPassword() {
		return password;
	}
	@JsonSetter
	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguages() {
		return languages;
	}


	public void setLanguages(String languages) {
		this.languages = languages;
	}

	public Set<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}
}