package com.nexym.worqat.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name= "review")
public class Review {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "review_id")
	private long id;
	
	@NotBlank(message = "Reviewer is mandatory")
	@Column(columnDefinition="TEXT", name = "reviewer_name")
	private String reviewer;
	
	@NotBlank(message = "Photo is mandatory")
	@Column(name = "reviewer_photo")
	private String photoReviewer;
	

	@NotBlank(message = "*Please provide your review")
	@Column(name = "text_review")
	private String textReview;
	
	@Column(name = "review_date")
	private String createdAt;
	
	@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;
	
	@NotNull
	@Column(name="rate")
	private int rate;
	
	@OneToMany(mappedBy = "review")
	@Column(name = "comments")
	private List<Comment> comments = new ArrayList<>();
	
	
	
	public Review() {
		super();
	}

	public Review(long id, @NotBlank(message = "Reviewer is mandatory") String reviewer,
			@NotBlank(message = "Photo is mandatory") String photoReviewer,
			@NotBlank(message = "*Please provide your review") String textReview, String createdAt, User user,
			@NotNull int rate, List<Comment> comments) {
		super();
		this.id = id;
		this.reviewer = reviewer;
		this.photoReviewer = photoReviewer;
		this.textReview = textReview;
		this.createdAt = createdAt;
		this.user = user;
		this.rate = rate;
		this.comments = comments;
	}

	public String getPhotoReviewer() {
		return photoReviewer;
	}

	public void setPhotoReviewer(String photoReviewer) {
		this.photoReviewer = photoReviewer;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public List<Comment> getComments() {
		return comments;
	}


	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTextReview() {
		return textReview;
	}

	public void setTextReview(String textReview) {
		this.textReview = textReview;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getReviewer() {
		return reviewer;
	}

	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}
	
	
	
}
