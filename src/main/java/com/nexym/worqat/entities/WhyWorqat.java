package com.nexym.worqat.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "whyworqat")
public class WhyWorqat {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
		//@NotBlank(message = "Description1 is mandatory")
		@Column(name = "Description1", columnDefinition="TEXT") 
		private String description1;
		
		//@NotBlank(message = "Description2 is mandatory")
		@Column(name = "Description2", columnDefinition="TEXT") 
		private String description2;
		
		//@NotBlank(message = "Description3 is mandatory")
		@Column(name = "Description3", columnDefinition="TEXT") 
		private String description3;
		
		//@NotBlank(message = "Description3 is mandatory")
		@Column(name = "Description4", columnDefinition="TEXT") 
		private String description4;

		public WhyWorqat(String description1, String description2, String description3, String description4) {
			super();
			this.description1 = description1;
			this.description2 = description2;
			this.description3 = description3;
			this.description4 = description4;
		}

		public WhyWorqat() {
			super();
			// TODO Auto-generated constructor stub
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getDescription1() {
			return description1;
		}

		public void setDescription1(String description1) {
			this.description1 = description1;
		}

		public String getDescription2() {
			return description2;
		}

		public void setDescription2(String description2) {
			this.description2 = description2;
		}

		public String getDescription3() {
			return description3;
		}

		public void setDescription3(String description3) {
			this.description3 = description3;
		}

		public String getDescription4() {
			return description4;
		}

		public void setDescription4(String description4) {
			this.description4 = description4;
		}
		
}
