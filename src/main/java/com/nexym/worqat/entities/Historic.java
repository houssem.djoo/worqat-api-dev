package com.nexym.worqat.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "historic")
public class Historic {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "year")
	private int year ;
	
	@Column(name = "status_projects")
	private String projects ;
	
	@Column(name = "january")
	private int january ;
	
	@Column(name = "february")
	private int february ;
	
	@Column(name = "march")
	private int march ;
	
	@Column(name = "april")
	private int april ;
	
	@Column(name = "may")
	private int may ;
	
	@Column(name = "june")
	private int june ;
	
	@Column(name = "july")
	private int july ;
	
	@Column(name = "august")
	private int august ;
	
	@Column(name = "september")
	private int september ;
	
	@Column(name = "october")
	private int october ;
	
	@Column(name = "november")
	private int november ;
	
	@Column(name = "december")
	private int december ;

	public Historic() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Historic(int year, String projects, int january, int february, int march, int april, int may,
			int june, int july, int august, int september, int october, int november, int december) {
		super();
		this.year = year;
		this.projects = projects;
		this.january = january;
		this.february = february;
		this.march = march;
		this.april = april;
		this.may = may;
		this.june = june;
		this.july = july;
		this.august = august;
		this.september = september;
		this.october = october;
		this.november = november;
		this.december = december;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getProjects() {
		return projects;
	}

	public void setProjects(String projects) {
		this.projects = projects;
	}

	public int getJanuary() {
		return january;
	}

	public void setJanuary(int january) {
		this.january = january;
	}

	public int getFebruary() {
		return february;
	}

	public void setFebruary(int february) {
		this.february = february;
	}

	public int getMarch() {
		return march;
	}

	public void setMarch(int march) {
		this.march = march;
	}

	public int getApril() {
		return april;
	}

	public void setApril(int april) {
		this.april = april;
	}

	public int getMay() {
		return may;
	}

	public void setMay(int may) {
		this.may = may;
	}

	public int getJune() {
		return june;
	}

	public void setJune(int june) {
		this.june = june;
	}

	public int getJuly() {
		return july;
	}

	public void setJuly(int july) {
		this.july = july;
	}

	public int getAugust() {
		return august;
	}

	public void setAugust(int august) {
		this.august = august;
	}

	public int getSeptember() {
		return september;
	}

	public void setSeptember(int september) {
		this.september = september;
	}

	public int getOctober() {
		return october;
	}

	public void setOctober(int october) {
		this.october = october;
	}

	public int getNovember() {
		return november;
	}

	public void setNovember(int november) {
		this.november = november;
	}

	public int getDecember() {
		return december;
	}

	public void setDecember(int december) {
		this.december = december;
	}
	
	
	
}
