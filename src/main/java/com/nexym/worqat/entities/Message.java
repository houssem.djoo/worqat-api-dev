package com.nexym.worqat.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "message")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotBlank(message = "username is mandatory")
	@Column(name = "user_sender")
	private String sender;
	
	//@NotBlank(message = "user sender photo is mandatory")
	@Column(name = "user_sender_photo")
	private String senderPhoto;
	
	@NotBlank(message = "message is mandatory")
	@Column(columnDefinition="TEXT")
	private String content;
	
	@NotBlank(message = "user reciever is mandatory")
	@Column(name = "user_reciever")
	private String reciever;
	
	//@NotBlank(message = "user photo is mandatory")
	@Column(name = "user_reciever_photo")
	private String recieverPhoto;
	
	@JsonIgnore
	@NotNull
	@ManyToOne
    @JoinColumn(name = "conversation_id", referencedColumnName = "conversation_id")
	private Conversation conversation;
	
	@Column(name = "date_msg")
	private String createdAt;

	public Message() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Message(@NotBlank(message = "username is mandatory") String sender,
			@NotBlank(message = "message is mandatory") String content,
			@NotBlank(message = "user reciever is mandatory") String reciever, String createdAt) {
		super();
		this.sender = sender;
		this.content = content;
		this.reciever = reciever;
		this.createdAt = createdAt;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReciever() {
		return reciever;
	}

	public void setReciever(String reciever) {
		this.reciever = reciever;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	public String getSenderPhoto() {
		return senderPhoto;
	}

	public void setSenderPhoto(String senderPhoto) {
		this.senderPhoto = senderPhoto;
	}

	public String getRecieverPhoto() {
		return recieverPhoto;
	}

	public void setRecieverPhoto(String recieverPhoto) {
		this.recieverPhoto = recieverPhoto;
	}

	
	
}