package com.nexym.worqat.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Entity
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "project_id")
	private long id;
	
	@NotBlank(message = "Name is mandatory")
	@Column(name = "project_title")
	private String title;

	@NotBlank(message = "Description is mandatory")
	@Column(name = "description")
	private String description;
	
	@NotBlank(message = "Category is mandatory")
	@Column(name = "category")
	private String category;
	
	@NotNull(message = "Duration is mandatory")
	@Column(name = "duration")
	private int duration;
	
	@NotNull(message = "Price is mandatory")
	@Column(name = "price")
	private float price;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "project_skills",
	joinColumns = { @JoinColumn(name = "project_id")},
	inverseJoinColumns = { @JoinColumn (name = "skill_id")})
	private Set<Skill> skillsReq = new HashSet<>();
	
	@ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;
	
	//@NotBlank(message = "AttatchmentFile is mandatory")
	//@ElementCollection
	@Column(name = "attachment_urls")
	private ArrayList<String> attachmentUrls;
	
	@Column(name = "status")
	private String status;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "projects_submits",
	joinColumns = { @JoinColumn(name = "project_id")},
	inverseJoinColumns = { @JoinColumn (name = "submit_project_id")})
	private Set<SubmitsRequest> freelancersSubmits = new HashSet<>();
	
	@Column
	private String freelancerAffected;
	
	@Column
	private String createdAt ;
	
	@Column
	private Date beginningWork ;

	public Project(long id, @NotBlank(message = "Name is mandatory") String title,
			@NotBlank(message = "Description is mandatory") String description,
			@NotBlank(message = "Category is mandatory") String category,
			@NotNull(message = "Duration is mandatory") int duration,
			@NotNull(message = "Price is mandatory") float price, Set<Skill> skillsReq, User user,
			ArrayList<String> attachmentUrls, String status, Set<SubmitsRequest> freelancersSubmits,
			String freelancerAffected, String createdAt, Date beginningWork) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.category = category;
		this.duration = duration;
		this.price = price;
		this.skillsReq = skillsReq;
		this.user = user;
		this.attachmentUrls = attachmentUrls;
		this.status = status;
		this.freelancersSubmits = freelancersSubmits;
		this.freelancerAffected = freelancerAffected;
		this.createdAt = createdAt;
		this.beginningWork = beginningWork;
	}

	public void removeSubmit(SubmitsRequest submit) {
		freelancersSubmits.remove(submit);
	}
	
	public void removeAllSubmits() {
		freelancersSubmits = new HashSet<>();
	}
	
	public void removeAllSkills() {
		skillsReq = new HashSet<>();
	}
	@Override
	public String toString() {
		return "Project [title=" + title + ", description=" + description + ", category=" + category + ", duration="
				+ duration + ", price=" + price + ", skillsReq=" + skillsReq + ", user=" + user + ", attachmentUrls="
				+ attachmentUrls + ", status=" + status + ", freelancersSubmits=" + freelancersSubmits
				+ ", freelancerAffected=" + freelancerAffected + "]";
	}


	public Project() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Date getBeginningWork() {
		return beginningWork;
	}

	public void setBeginningWork(Date beginningWork) {
		this.beginningWork = beginningWork;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Set<SubmitsRequest> getFreelancersSubmits() {
		return freelancersSubmits;
	}

	public String getFreelancerAffected() {
		return freelancerAffected;
	}

	public void setFreelancerAffected(String freelancerAffected) {
		this.freelancerAffected = freelancerAffected;
	}

	public void setFreelancersSubmits(Set<SubmitsRequest> freelancersSubmits) {
		this.freelancersSubmits = freelancersSubmits;
	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public float getPrice() {
		return price;
	}


	public void setPrice(float price) {
		this.price = price;
	}


	public Set<Skill> getSkillsReq() {
		return skillsReq;
	}


	public void setSkillsReq(Set<Skill> skillsReq) {
		this.skillsReq = skillsReq;
	}

	public ArrayList<String> getAttachmentUrls() {
		return attachmentUrls;
	}

	public void setAttachmentUrls(ArrayList<String> attachmentUrls) {
		this.attachmentUrls = attachmentUrls;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
