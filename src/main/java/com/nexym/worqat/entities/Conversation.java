package com.nexym.worqat.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Conversation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "conversation_id")
	private long id;
	
	@NotBlank(message = "Conversation Name is mandatory")
	@Column(name = "conversation_name")
	private String conversationName;
	
	@Column(name = "created_at")
	private Date createdAt;
	
	@Column(name = "last_msg_date")
	private Date lastMsg;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "conversations_users",
	joinColumns = { @JoinColumn(name = "conversation_id")},
	inverseJoinColumns = { @JoinColumn (name = "user_id")})
	private Set<User> users = new HashSet<>();
	
	@OneToMany(mappedBy = "conversation")
	@Column(name = "messages")
	private List<Message> messages = new ArrayList<>();
	
	@NotBlank
	@Column
	private String freelancerName ;
	
	@NotBlank
	@Column
	private String ownerName ;
	
	@NotBlank
	@Column
	private String projectName ;
	
	public Conversation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Conversation(long id, @NotBlank(message = "Conversation Name is mandatory") String conversationName,
			Date createdAt, Date lastMsg, Set<User> users, List<Message> messages, @NotBlank String freelancerName,
			@NotBlank String ownerName, @NotBlank String projectName) {
		super();
		this.id = id;
		this.conversationName = conversationName;
		this.createdAt = createdAt;
		this.lastMsg = lastMsg;
		this.users = users;
		this.messages = messages;
		this.freelancerName = freelancerName;
		this.ownerName = ownerName;
		this.projectName = projectName;
	}



	public Date getLastMsg() {
		return lastMsg;
	}

	public void setLastMsg(Date lastMsg) {
		this.lastMsg = lastMsg;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getFreelancerName() {
		return freelancerName;
	}


	public void setFreelancerName(String freelancerName) {
		this.freelancerName = freelancerName;
	}


	public String getOwnerName() {
		return ownerName;
	}


	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getConversationName() {
		return conversationName;
	}

	public void setConversationName(String conversationName) {
		this.conversationName = conversationName;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	
}
