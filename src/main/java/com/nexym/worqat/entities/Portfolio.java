package com.nexym.worqat.entities;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="portfolio")
public class Portfolio {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotBlank(message = "title is mandatory")
	@Column(name = "title")
	private String title;
	
	@Column(name = "attachment_url")
	private ArrayList<String> attachmentUrl;

	public Portfolio() {
	}

	
	public Portfolio(@NotBlank(message = "title is mandatory") String title,
			ArrayList<String> attachmentUrl) {
		this.title = title;
		this.attachmentUrl = attachmentUrl;
	}



	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ArrayList<String> getAttachmentUrl() {
		return attachmentUrl;
	}

	public void setAttachmentUrl(ArrayList<String> attachmentUrl) {
		this.attachmentUrl = attachmentUrl;
	}


	/**** Many To One ****/
	@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

}
