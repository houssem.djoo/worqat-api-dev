package com.nexym.worqat.entities;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "banner")
public class Banner {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "banner_urls")
	private ArrayList<String> bannerUrls ;

	public Banner(ArrayList<String> bannerUrls) {
		super();
		this.bannerUrls = bannerUrls;
	}

	public Banner() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ArrayList<String> getBannerUrls() {
		return bannerUrls;
	}

	public void setBannerUrls(ArrayList<String> bannerUrls) {
		this.bannerUrls = bannerUrls;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
