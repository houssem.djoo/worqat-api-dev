package com.nexym.worqat.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table (name = "submits")
public class SubmitsRequest {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "submit_id")
	private long id;
	
	@NotNull(message = "price per hour is mandatory")
	@Column(name = "price_per_hour")
	private float pricePerHour;
	
	@NotNull(message = "Hours per week is mandatory")
	@Column(name = "weekly_hours")
	private float weeklyHours;
	
	@NotBlank(message = "Name is mandatory")
	@Column(name = "message_describe")
	private String messageDescription;
	
	@ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;
	
	/*@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "submit_project_id", referencedColumnName = "project_id")
	private Project project;
	
	public void removeProject() {
		project=null;
	}*/
	/*@JsonIgnore
	@NotNull(message = "ProjectID is mandatory")
	@Column(name = "project_id")
	private long projectId;
	
	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
*/
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getPricePerHour() {
		return pricePerHour;
	}

	public void setPricePerHour(float pricePerHour) {
		this.pricePerHour = pricePerHour;
	}

	public float getWeeklyHours() {
		return weeklyHours;
	}

	public void setWeeklyHours(float weeklyHours) {
		this.weeklyHours = weeklyHours;
	}

	public String getMessageDescription() {
		return messageDescription;
	}

	public void setMessageDescription(String messageDescription) {
		this.messageDescription = messageDescription;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	*/
	

}
