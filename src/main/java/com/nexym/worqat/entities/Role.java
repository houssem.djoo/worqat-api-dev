package com.nexym.worqat.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="role", uniqueConstraints = {
		@UniqueConstraint(columnNames = "role")})

public class Role {
	public Role(String role) {
		super();
		this.role = role;
	}
	public Role() {
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="role_id")
	private int id ;
	
	@Column(name="role")
	@NotBlank
	private String role;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

}
