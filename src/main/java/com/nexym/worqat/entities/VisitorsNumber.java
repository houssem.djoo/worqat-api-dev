package com.nexym.worqat.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class VisitorsNumber {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column
	private long vistorsNumber;
	
	@Column
	private String ipsVisits ;
	
	public VisitorsNumber() {
		super();
		// TODO Auto-generated constructor stub
	}

	public VisitorsNumber(long vistorsNumber, String ipsVisits) {
		super();
		this.vistorsNumber = vistorsNumber;
		this.ipsVisits = ipsVisits;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public long getVistorsNumber() {
		return vistorsNumber;
	}

	public void setVistorsNumber(long vistorsNumber) {
		this.vistorsNumber = vistorsNumber;
	}

	public String getIpsVisits() {
		return ipsVisits;
	}

	public void setIpsVisits(String ipsVisits) {
		this.ipsVisits = ipsVisits;
	}

	
}
