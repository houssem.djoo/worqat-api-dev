package com.nexym.worqat.payload.response;

import java.util.HashSet;
import java.util.Set;

import com.nexym.worqat.entities.SubmitsRequest;

public class MessageResponse {
	private String freelancerAffected;
	
	private Set<SubmitsRequest> freelancersSubmits = new HashSet<>();
	
	public MessageResponse(String freelancerAffected, Set<SubmitsRequest> freelancersSubmits) {
		super();
		this.freelancerAffected = freelancerAffected;
		this.freelancersSubmits = freelancersSubmits;
	}

	public Set<SubmitsRequest> getFreelancersSubmits() {
		return freelancersSubmits;
	}

	public void setFreelancersSubmits(Set<SubmitsRequest> freelancersSubmits) {
		this.freelancersSubmits = freelancersSubmits;
	}

	public String getfreelancerAffected() {
		return freelancerAffected;
	}

	public void setfreelancerAffected(String freelancerAffected) {
		this.freelancerAffected = freelancerAffected;
	}
}

