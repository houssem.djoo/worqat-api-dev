package com.nexym.worqat.payload.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class ForgotPasswordRequest {


		@NotBlank
		@Email
		private String email;


		public String getEmail() {
			return email;
		}

}