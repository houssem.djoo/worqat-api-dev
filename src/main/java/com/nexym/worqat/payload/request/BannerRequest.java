package com.nexym.worqat.payload.request;

import java.util.ArrayList;

public class BannerRequest {
	
	private ArrayList<String> bannerUrls;

	public ArrayList<String> getBannerUrls() {
		return bannerUrls;
	}

	public void setBannerUrls(ArrayList<String> bannerUrls) {
		this.bannerUrls = bannerUrls;
	}
	
}
