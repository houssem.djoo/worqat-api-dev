package com.nexym.worqat.payload.request;

import javax.validation.constraints.NotBlank;

public class NewPasswordSend {
	
	@NotBlank
	private String password ;
	
	@NotBlank
	private String passwordConfirmation ;

	public String getPassword() {
		return password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	
}
