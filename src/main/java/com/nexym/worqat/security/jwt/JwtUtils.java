package com.nexym.worqat.security.jwt;



import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.nexym.worqat.entities.User;
import com.nexym.worqat.repositories.UserRepository;
import com.nexym.worqat.security.services.UserDetailsImpl;

import io.jsonwebtoken.*;

@Component
public class JwtUtils {
	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	private String jwtSecret="secret10";

	private int jwtExpirationMs;
	
	@Autowired
	private UserRepository userRepository;
	
	public String generateJwtToken(Authentication authentication) {

		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
		
	     List<String> roles = userPrincipal.getAuthorities().stream()
	                .map(item -> item.getAuthority())
	                .collect(Collectors.toList());
	     Algorithm algo = Algorithm.HMAC256("secret10");
	     
	     User user = userRepository.findUserByUsername(userPrincipal.getUsername());
		
		return 
		JWT.create().withSubject(userPrincipal.getUsername())
				 	.withIssuedAt(new Date(System.currentTimeMillis()))
				 	.withClaim("roles",userPrincipal.getAuthorities().stream().map(ga->ga.getAuthority()).collect(Collectors.toList()))
				 	.sign(algo);
	}
	public String generateJwtGoogleSign(String email) {
		User user = userRepository.findUserByEmail(email);
	     Algorithm algo = Algorithm.HMAC256("secret10");
	    	
		return 
		JWT.create().withSubject(user.getUsername())
				 	.withIssuedAt(new Date(System.currentTimeMillis()))
				 	.withClaim("roles",user.getRoles().stream().map(rl->rl.getRole()).collect(Collectors.toList()))
				 	.sign(algo);
	}

	public String getUserNameFromJwtToken(String token) {
		return Jwts.parser().setSigningKey("secret10").parseClaimsJws(token).getBody().getSubject();
	}

	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			logger.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			logger.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}
}
